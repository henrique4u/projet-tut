<?php
/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 22/06/2015
 * Time: 09:20
 */
namespace models;
class Image extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'image';
    protected $primaryKey = 'id_image';
    public $timestamps = false;

}