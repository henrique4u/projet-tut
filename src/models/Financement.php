<?php
/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 22/06/2015
 * Time: 09:26
 */

namespace models;


class Financement extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'financement';
    protected $primaryKey = 'id_fi';
    public $timestamps = false;

}