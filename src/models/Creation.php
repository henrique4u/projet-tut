<?php
/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 22/06/2015
 * Time: 09:24
 */

namespace models;


class Creation extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'creation';
    protected $primaryKey = 'id_user, id_projet';
    public $timestamps = false;

}