<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 22/06/2015
 * Time: 09:18
 */

namespace models;

class Partenaire extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'partenaire';
    protected $primaryKey = 'id_partenaire';
    public $timestamps = false;

    public function projet(){
        return $this->belongsToMany('models\Projet');
    }
}