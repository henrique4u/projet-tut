<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 22/06/2015
 * Time: 09:13
 */

namespace models;

class Projet extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'projet';
    protected $primaryKey = 'id_projet';
    public $timestamps = false;

    /** Recherche les cadeaux d'un projet
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cadeaux(){
        return $this->hasMany('models\Cadeau', 'id_projet');
    }

    /** Recherche les differents partenaires d'un projet
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function partenaire(){
        return $this->belongsToMany('models\Partenaire');
    }

    /** Recherche la categorie d'un projet
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function categorie(){
        return $this->hasOne('models\Categorie', 'id_categorie', 'id_categorie');
    }

}