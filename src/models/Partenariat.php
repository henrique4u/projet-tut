<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 22/06/2015
 * Time: 09:17
 */

namespace models;

class Partenariat extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'partenariat';
    protected $primaryKey = 'id_projet, id_partenaire';
    public $timestamps = false;

}