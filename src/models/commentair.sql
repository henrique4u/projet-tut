-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 13 Octobre 2015 à 09:32
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projet-tut`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentair`
--

CREATE TABLE IF NOT EXISTS `commentair` (
  `id_commentair` int(11) NOT NULL AUTO_INCREMENT,
  `id_projet` int(11) NOT NULL,
  `id_reponce` int(11) NOT NULL,
  `commentaire` text NOT NULL,
  `stage` int(2) NOT NULL,
  `num_rep` int(11) NOT NULL,
  PRIMARY KEY (`id_commentair`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `commentair`
--

INSERT INTO `commentair` (`id_commentair`, `id_projet`, `id_reponce`, `commentaire`, `stage`, `num_rep`) VALUES
(1, 22, -1, 'Question 1', 0, -1),
(2, 22, 1, 'Reponce 1', 1, 0),
(3, 22, 1, 'reponce 2', 1, 1),
(4, 22, 3, 'reponce 2.1', 2, 0),
(5, 22, 1, 'reponce 3', 1, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
