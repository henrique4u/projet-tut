<?php
/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 22/06/2015
 * Time: 09:22
 */

namespace models;

class Categorie extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'categorie';
    protected $primaryKey = 'id_categorie';
    public $timestamps = false;

    public function projets(){
        return $this->hasMany('models\Projet');
    }

}