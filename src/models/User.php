<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 22/06/2015
 * Time: 09:17
 */

namespace models;

class User extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'user';
    protected $primaryKey = 'id_user';
    public $timestamps = false;

}