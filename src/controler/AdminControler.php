<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 22/06/2015
 * Time: 14:16
 */

namespace controler;


use models\Cadeau;
use models\Comentair;
use models\Creation;
use models\Financement;
use models\Image;
use models\Partenaire;
use models\Partenariat;
use models\Projet;
use models\User;
use Slim\Slim;
use vue\Form;
use vue\render;

class AdminControler {

    public function connect(){
        $app = Slim::getInstance();
        $pseudo = filter_var($app->request->post('pseudo'), FILTER_SANITIZE_STRING);
        $pswd = filter_var($app->request->post('pswd'), FILTER_SANITIZE_STRING);
        $usr = User::where("login", "=", $pseudo)->get();
        foreach($usr as $user){
            if(password_verify($pswd, $user->pswd)){
                $_SESSION['user'] = $user;
                $app->redirect($app->urlFor("accueil"));
            }
        }
        $app->redirect($app->urlFor("formCompte") . "?err=2");
    }

    public function disconnect(){
        $_SESSION = array();
        session_destroy();
        $app = Slim::getInstance();
        $app->redirect("http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']));
    }

    public function addProjet(){
        $app = Slim::getInstance();
        $_SESSION['projetTmp']->save();

        $proj = $_SESSION['projetTmp'];
        $fis = \models\Partenaire::all();

        $urlProj = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/association/" . $proj->id_projet . '/';
var_dump($_POST);
        foreach($fis as $part){
            if(isset($_SESSION['post']['check'.$part->id_partenaire])){

                $contenu = 'Pour valider l\'accord d\'association à ce projet, cliquer sur ce <a href="' . $urlProj . $part->id_partenaire . '">lien</a>';
                $this->mailer("Demande de partenariat avec le projet ".$proj->nom_projet, $contenu, $part->mail_partenaire);
            }
        }

        $_SESSION['post'] = array();
        $compteur = 1;
        while(isset($_POST["nomCadeau".$compteur])){
            $cadeau = new Cadeau();
            $cadeau->nom_cadeau = filter_var($app->request->post("nomCadeau".$compteur), FILTER_SANITIZE_STRING);
            $cadeau->description = $app->request->post("descr".$compteur);
            $cadeau->somme_a_atteindre = filter_var($app->request->post("sommmeCadeau".$compteur), FILTER_SANITIZE_NUMBER_INT);
            $cadeau->nb_dispo = filter_var($app->request->post("nbCadeau".$compteur), FILTER_SANITIZE_NUMBER_FLOAT);
            $cadeau->id_projet = $_SESSION['projetTmp']->id_projet;
            $cadeau->save();
            $compteur++;
        }
        $crea = new Creation();
        $crea->id_user = $_SESSION['user']['id_user'];
        $crea->id_projet = $_SESSION['projetTmp']->id_projet;
        $crea->save();

        $idplop = $_SESSION['projetTmp']->id_projet;

        $_SESSION['projetTmp'] = null;
        $app->redirect(str_replace(":id",$idplop ,$app->urlFor("projetId")));


    }

    public function creerCompte(){
        $app = Slim::getInstance();


        $nom = filter_var($app->request->post("nom"), FILTER_SANITIZE_STRING);
        $prenom = filter_var($app->request->post("prenom"), FILTER_SANITIZE_STRING);
        $mail = filter_var($app->request->post("email"), FILTER_SANITIZE_EMAIL);
        $date_naiss = filter_var($app->request->post("dateNaiss"), FILTER_SANITIZE_STRING);

        $login = filter_var($app->request->post("newlogin"), FILTER_SANITIZE_STRING);
        $pswd = password_hash($app->request->post("newpswd"), PASSWORD_DEFAULT, array("cost"=>12));
        $rules=0;

        if(!is_null(User::where("login", "=", $login)->get()->get(0))){
            $app->redirect($app->urlFor("formCompte") . "?err=1");
        }elseif(!is_null(User::where("mail", "=", $mail)->get()->get(0))){
            $app->redirect($app->urlFor("formCompte") . "?err=1");
        }else{
            $user=new User();
            $user->nom=$nom;
            $user->prenom=$prenom;
            $user->mail=$mail;
            $user->date_naiss = $date_naiss;
            $user->login=$login;
            $user->pswd=$pswd;
            $user->rules=$rules;

            $user->save();

            $_SESSION['user'] = $user;

            $msg = "Vous venez de vous inscrire sur le site NomSite<br>
                    nom: " . $nom . "<br>Prenom: " . $prenom . "<br>Date de naissance: ".
                    $date_naiss . "<br>Login: " . $login;

            $this->mailer("Inscription sur le site NomSite", $msg, $mail);

            $app->redirect($app->urlFor("accueil"));
        }

    }

    public function financer($idProj, $somme){
        if(isset($_SESSION['user'])){
            $proj = Projet::find($idProj);
            if(!is_null($proj)){
                $fi = new Financement;
                $fi->cadeau_obtenu=-1;

                $lCadeau = Cadeau::where('id_projet', "=", $idProj)->get();

                //cadeau de l'utilisateur
                $cadeau=new Cadeau();
                $cadeau->somme_a_atteindre=-1;

                foreach($lCadeau as $lcad_courant){
                    if($lcad_courant->somme_a_atteindre<=$somme &&
                        $cadeau->somme_a_atteindre<$lcad_courant->somme_a_atteindre){
                        if($lcad_courant->nb_dispo>=1){
                            $cadeau=$lcad_courant;
                        }
                    }
                }

                if($cadeau->somme_a_atteindre!=-1){
                    $cadeau->nb_dispo--;
                    $fi->cadeau_obtenu=$cadeau->id_cadeau;
                    $cadeau->save();
                }

                $fi->somme_donnee = $somme;
                $fi->date_fi = date('Y-m-d');
                $fi->id_user =$_SESSION['user']->id_user;
                $fi->id_projet = $idProj;
                $fi->save();

                $proj->somme_courente += $somme;
                $proj->save();

                $app = Slim::getInstance();
                $app->redirect(str_replace(":id", $idProj ,Slim::getInstance()->urlFor("projetId")));

            }else{
                $render = new render();
                $render->notFound('
                 <div class="section"><div class="container"><div class="row"><div class="col-md-2"></div> <div class="col-md-8 text-center alert alert-dismissable alert-warning">
                      Err dans la recherce du projet
                 </div></div></div></div>

            ');
            }

        }else{
            $render = new render();
            $render->notFound('
                 <div class="section"><div class="container"><div class="row"><div class="col-md-2"></div> <div class="col-md-8 text-center alert alert-dismissable alert-warning">
                       Vous devez etre connectée pour financer un projet.
                 </div></div></div></div>

            ');
        }
    }

    public function changeMdp(){

        $oldPasswd = filter_var($_POST['oldPasswd'], FILTER_SANITIZE_STRING);
        $newPasswd = filter_var($_POST['newPasswd'], FILTER_SANITIZE_STRING);
        $verifPasswd = filter_var($_POST['verifNewPasswd'], FILTER_SANITIZE_STRING);
        $app = Slim::getInstance();
        if(password_verify( $oldPasswd, $_SESSION['user']['pswd'])){
            if($newPasswd == $verifPasswd){
                $_SESSION['user']['pswd'] = password_hash($newPasswd, PASSWORD_DEFAULT, array("cost"=>12));
                $usr = User::find($_SESSION['user']['id_user']);
                $usr->pswd = $_SESSION['user']['pswd'];
                $usr->save();

                $app->redirect($app->urlFor("changeMdp") . "?success=1");
            }else{
                $app->redirect($app->urlFor("changeMdp") . "?err=1");
            }
        }else{
            $app->redirect($app->urlFor("changeMdp") . "?err=2");
        }
    }

    public function changeMail(){
        $app = Slim::getInstance();
        $mail = filter_var($_POST['mail'], FILTER_SANITIZE_STRING);
        $pswd = filter_var($_POST['pswd'], FILTER_SANITIZE_STRING);
        if(password_verify( $pswd, $_SESSION['user']['pswd'])){
            $_SESSION['user']['mail'] = $mail;
            $usr = User::find($_SESSION['user']['id_user']);
            $usr->mail = $_SESSION['user']['mail'];
            $usr->save();
            $app->redirect($app->urlFor("changeMail") . "?success=1");
        }else{
            $app->redirect($app->urlFor("changeMail") . "?err=1");
        }
    }

    public function creerPartenaire(){
        $app = Slim::getInstance();


        $nom = filter_var($app->request->post("nom_partenaire"), FILTER_SANITIZE_STRING);
        $mail = filter_var($app->request->post("email_partenaire"), FILTER_SANITIZE_EMAIL);
        $adresse = filter_var($app->request->post("adresse_partenaire"), FILTER_SANITIZE_STRING);

        if(!is_null(Partenaire::where("nom_partenaire", "=", $nom)->get()->get(0))){
            $app->redirect($app->urlFor("creationPartenaire") . "?err");
        }else{
            $partenaire = new Partenaire();
            $partenaire->nom_partenaire = $nom;
            $partenaire->mail_partenaire = $mail;
            $partenaire->adresse_partenaire = $adresse;
            $img = new Image();
            $img->save();
            $partenaire->id_image = $img->id_image;
            if(move_uploaded_file($_FILES['logo_partenaire']['tmp_name'], "data/images/".$img->id_image . ".png"))

            $message = "Bonjour,\r\nVotre demande de création de partenaire à bien été prit en compte : \r\n
            - nom de l'entreprise : $nom\r\n-mail : $mail\r\n- adresse : $adresse ";
            $this->mailer('ajout partenaire', $message, $mail);

            $partenaire->save();

            $_SESSION['partenaire'] = $partenaire;

            $vue = new render();
            $vue->header();
            $url = $app->urlFor("accueil");
            echo <<<END
            <div class="section"><div class="container"><div class="row"><div class="col-md-2"></div> <div class="col-md-8 text-center alert alert-dismissable alert-success">
                Le partenaire a bien etait créé.<br>Vous allez etre rediriger dans <span id='timer'>5</span> secondes.
            </div></div></div></div>
            <script>
                var compt = 5;
                function timeOutCustom(){
                    setTimeout(function (){Redirect()},1000);
                }
                function Redirect()
                {
                    compt--;
                    var plop = ""+compt;
                    if(compt!=-1){
                        $("#timer").empty().append(plop);
                        timeOutCustom();
                    }else
                        window.location="$url";
                }

                $(window).load(function() {
                    timeOutCustom()
                });
            </script>
END;
        }

    }

    public function repondre(){
        $com = $_POST['com'];
        $idProj = $_POST['id_proj'];
        $idRep = $_POST['id_reponce'];
        $numRep = $_POST['num_rep'];

        $commentaire = new Comentair();
        $commentaire->id_projet = $idProj;
        $commentaire->id_reponce = $idRep;
        $commentaire->stage = 1;
        $commentaire->commentaire = $com;
        $commentaire->num_rep = $numRep;
        $commentaire->save();

        $urlRedir = Slim::getInstance()->urlFor("projetId");
        $urlRedir = str_replace(":id", $idProj, $urlRedir) . "#c". $commentaire->id_reponce . "r" . ($commentaire->num_rep +1);
        Slim::getInstance()->redirect($urlRedir);
    }

    public function question(){
    $com = $_POST['com'];
    $idProj = $_POST['id_proj'];

    $commentaire = new Comentair();
    $commentaire->id_projet = $idProj;
    $commentaire->id_reponce = -1;
    $commentaire->stage = 0;
    $commentaire->commentaire = $com;
    $commentaire->num_rep = -1;
    $commentaire->save();

    $urlRedir = Slim::getInstance()->urlFor("projetId");
    $urlRedir = str_replace(":id", $idProj, $urlRedir);
    Slim::getInstance()->redirect($urlRedir);
}

    public function modifProjet($id)
{
    $crea = Creation::where("id_projet", "=", $id)->get()->toArray();
    if($crea[0]['id_user'] != $_SESSION['user']->id_user && $_SESSION['user']->rules < 1){
        $vue = new render();
        $vue->notFound('
                <div class="section"><div class="container"><div class="row"><div class="col-md-2"></div> <div class="col-md-8 text-center alert alert-dismissable alert-warning">
                    Ce projet ne vous appartient pas.
                </div></div></div></div>
            ');
    }else{
        $projet = Projet::find($id);
        $projet->nom_projet = $_POST['nom'];
        $projet->somme_a_collecter = $_POST['Somme'];
        $projet->description = $_POST['descr'];
        $projet->date_fin = $_POST['dateFi'];
        $projet->save();
        $vue = new render();
        $vue->header();
        $url = str_replace(":id", $id, Slim::getInstance()->urlFor("projetId"));

        $fis = \models\Financement::where("id_projet", "=", $id)->select("id_user")->distinct()->get();
        $urlProj = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/projet/" . $id;
        $contenu = 'Pour vois le projet, cliquer sur ce <a href="' . $urlProj . '">lien</a>';
        $proj = $_POST['nom'];
        foreach($fis as $fi){
            $user = \models\User::find($fi->id_user);
            $this->mailer("Le projet $proj a été modifié", $contenu, $user->mail);
        }

        echo <<<END
            <div class="section"><div class="container"><div class="row"><div class="col-md-2"></div> <div class="col-md-8 text-center alert alert-dismissable alert-success">
                Votre projet a était mis a jour.<br>Vous allez etre rediriger dans <span id='timer'>5</span> secondes.
            </div></div></div></div>
            <script>
                var compt = 5;
                function timeOutCustom(){
                    setTimeout(function (){Redirect()},1000);
                }
                function Redirect()
                {
                    compt--;
                    var plop = ""+compt;
                    if(compt!=-1){
                        $("#timer").empty().append(plop);
                        timeOutCustom();
                    }else
                        window.location="$url";
                }

                $(window).load(function() {
                    timeOutCustom()
                });
            </script>
END;


    }
}

    public function mailer($sujet,$contenu,$mail_destinataire){

        $mail             = new \PHPMailer();

        $mail->IsSMTP(); // telling the class to use SMTP
        //$mail->Host       = "mail.yourdomain.com"; // SMTP server
        //$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        //l$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
        $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username   = "projetlimaga@gmail.com";  // GMAIL username
        $mail->Password   = "plopplop";            // GMAIL password

        $mail->SetFrom('projetlimaga@gmail.com', 'No Reply');

        //$mail->AddReplyTo("name@yourdomain.com","First Last");

        $mail->Subject    = $sujet;

        // $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

        $mail->MsgHTML($contenu);

        $address = $mail_destinataire;
        $mail->AddAddress($address, "John Doe");

        $mail->Send();


    }

    public function associer($idProj,$idPart)
    {
        $partenariat= new Partenariat();

        $partenariat->id_projet=$idProj;
        $partenariat->id_partenaire=$idPart;
        $partenariat->save();

        $render = new render();
        $render->notFound('
                <div class="section"><div class="container"><div class="row"><div class="col-md-2"></div> <div class="col-md-8 text-center alert alert-dismissable alert-warning">
                    Votre partenariat a bien été pris en compte.
                </div></div></div></div>
            ');
    }

    public function delProjet($id){
        if(isset($_SESSION['user'])){
           /* if($_SESSION['user']->rules > 0){
                $crea = Creation::where("id_projet", "=", $id)->get();
                foreach($crea as $c){
                    $c->delete();
                }
                $cadeau = Cadeau::where("id_projet", "=", $id)->get();
                foreach($cadeau as $c){
                    $c->delete();
                }
                $proj = Projet::find($id);
                $proj->delete();
            }*/
        }
    }
}