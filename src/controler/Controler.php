<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 22/06/2015
 * Time: 09:13
 */

namespace controler;

use models\Cadeau;
use models\Categorie;
use models\Creation;
use models\Financement;
use models\Partenaire;
use models\Projet;
use Slim\Slim;
use vue\Form;
use vue\render;

class Controler {
    public function accueil(){
        $vue = new render();
        $vue->accueil();

    }

    public function afficheProjet($id){
        $vue = new render();
        $vue->afficherProjet($id);
    }

    public function formProjet(){
        $vue = new Form();
        $vue->addProjetForm();
    }

    public function formProjetPart2(){
        $vue = new Form();
        $vue->formCadeau();
    }

    public function formPartenaire(){
        $vue = new Form();
        $vue->creerPartenaire();
    }

    public function allProjet($id){
        $projet = Projet::all()->toArray();
        $projet = array_reverse ($projet);
        $nbPage = (int)(sizeof($projet)/4+1);
        if(sizeof($projet)%4 ==0)
            $nbPage--;
        $projet = array_slice($projet, ($id-1)*4, 4);
        $vue = new render();
        $vue->projetAll($id, $nbPage, $projet);
    }

    public function projectByCat($id, $page){
        $cat =Categorie::find($id);
        if($cat != null){
            $projet = Projet::where("id_categorie", "=", $id)->get()->toArray();
            $nbPage = (int)(sizeof($projet)/4+1);
            if($page>$nbPage){
                $app = Slim::getInstance();
                $prec = str_replace(":page", $nbPage, Slim::getInstance()->urlFor("projectByCat"));
                $prec = str_replace(":id", $id, $prec);
                $app->redirect($prec);
            }
            if(sizeof($projet)%4 ==0)
                $nbPage--;
            $projet = array_reverse($projet);
            $projet = array_slice($projet, ($page-1)*4, 4);


            $vue = new render();
            $vue->projectByCat($page, $nbPage, $projet, $cat);
        }else{
            $vue = new render();
            $vue->header();
            echo "<p><h2>oupsss</h2> la cathegorie que vous rechercher n'est pas disponible.</p>";
            $vue->footer();
        }
    }

    public function compte()
    {

        if (isset($_SESSION["user"])) {
            $tabProjFi = array();
            $tabMesProj = array();

            if($_SESSION['user']->rules == 0){
                $fi = Financement::where("id_user", "=", $_SESSION['user']['id_user'])->get()->toArray();
                foreach ($fi as $fiCour) {
                    $row['dateFi'] = $fiCour['date_fi'];
                    $proj = Projet::find($fiCour['id_projet']);
                    $row['nom_projet'] = $proj->nom_projet;
                    $row['don'] = $fiCour['somme_donnee'];
                    $row['idProj'] = $fiCour['id_projet'];
                    $row['cadeau'] = Cadeau::find($fiCour['cadeau_obtenu']);
                    $tabProjFi[] = $row;
                }

                $tadCrea = Creation::where("id_user", "=", $_SESSION['user']['id_user'])->get()->toArray();

                foreach ($tadCrea as $crea) {
                    $tabMesProj[] = Projet::find($crea['id_projet']);
                }
            }else{
                $proj = Projet::orderBy('date_debut', 'desc')->take(10)->get();
                foreach($proj as $p){
                    $tabMesProj[] = $p;
                }

                $proj = Projet::orderBy('date_debut', 'desc')->get();
                foreach($proj as $p){
                    $tabProjFi[] = $p;
                }
            }
            $render = new render();
            $render->profil($tabMesProj, $tabProjFi);
        }else{
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("formCompte"));
        }
    }

    private function notFound($err){
        (new render)->notFound($err);
    }

    public function modifProjet($id)
    {
        if(isset($_SESSION['user'])){
            $creation = Creation::where("id_projet", "=", $id)->get()->toArray();
            if($_SESSION['user']->rules == 1 || $creation[0]['id_user'] == $_SESSION['user']->id_user){
                $form = new Form();
                $projet = Projet::find($id);
                $form->changeProj($projet);
            }else{
                $this->notFound("Ce projet ne vous appartient pas");
            }
        }else{
            $this->notFound("Vous devez être connecté pour accéder à cette fonctionnalitée");
        }
    }

    public function search($sr)
    {
        $proj = \models\Projet::where('nom_projet', 'like', '%'. $sr .'%')->orderBy('id_projet', 'DESC')->get();
        return $proj->toArray();
    }
}