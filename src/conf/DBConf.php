<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 12/05/2015
 * Time: 14:41
 */

namespace conf;


class DBConf {
    public static function setConf(){
        $db = new \Illuminate\Database\Capsule\Manager();
        $db->addConnection(parse_ini_file('db.etuapp.conf.ini'));
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}