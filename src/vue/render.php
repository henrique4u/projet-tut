<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 22/06/2015
 * Time: 09:14
 */

namespace vue;

use models\Cadeau;
use models\Categorie;
use models\Comentair;
use models\Creation;
use models\Financement;
use models\Partenaire;
use models\Partenariat;
use models\Projet;
use models\User;
use Slim\Slim;

class render {

    public function accueil(){
        $this->header();

        $projets = Projet::where('id_projet', '>=', 0)->orderBy('id_projet', 'DESC')->get();
        $tabProjets = $projets->toArray();
        $baseImages = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/data/images/";
        $image1=$baseImages.$tabProjets[0]["id_image"].".png";
        $nom1 = $tabProjets[0]["nom_projet"];
        $description1 ="";

        $front = <<<END
    <div id="fullcarousel-example" data-interval="false" class="carousel slide"
    data-ride="carousel">
      <div class="carousel-inner">
        <div class="item">
          <img src="https://unsplash.imgix.net/photo-1421986527537-888d998adb74?fm=jpg&amp;s=e633562a1da53293c4dc391fd41ce41d">
          <div class="carousel-caption">
            <h2>Title</h2>
            <p>Description</p>
          </div>
        </div>
      </div>
       </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="height:800px; overflow:hidden">
            <div id="carousel-example" data-interval="5000" class="carousel slide"
            data-ride="carousel">
              <div class="carousel-inner">
                <div class="item active">
                  <img src="$image1">
                  <div class="carousel-caption descr-car">
                    <h2>$nom1</h2>
                    <p>$description1</p>
                  </div>
                </div>
END;

        $mid = "";
        for($i=1; $i<5 && isset($tabProjets[$i]); $i++) {

            $image=$baseImages.$tabProjets[$i]["id_image"].".png";
            $nom = $tabProjets[$i]["nom_projet"];
            $description = "";

            $mid = $mid."<div class=\"item\" >
                          <img src = \"".$image."\" >
                          <div class=\"carousel-caption descr-car\" >
                            <h2 >".$nom."</h2 >
                            <p >".$description."</p >
                            </div >
                            </div >";
        }

        $cat = Categorie::all();

        $p1 = "";
        $p2 = "";
        $active = "active in";

        foreach($cat as $categ){
            $projets = Projet::where('id_categorie', "=", $categ->id_categorie)->orderBy('id_projet', 'DESC')->get();
            $projet = $projets->get(0);
            if(!is_null($projet)){
                $descr = $projet->description;
                $pourcent = $projet->somme_courente / $projet->somme_a_collecter * 100;
                $sommeACollecter = $projet->somme_a_collecter;
                $idProjet = $projet->id_projet;
                $nomCat = $categ->nom_categorie;
                $urlProj = str_replace(":id", $projet->id_projet ,Slim::getInstance()->urlFor("projetId"));


                $p1 = $p1 . "
              <div class=\"tab-pane fade $active \" id=\"$idProjet\">

                <div style=\"height: 250px; overflow: hidden;\">$descr</div>
                <a class=\"btn btn-primary\" href=\"$urlProj\">Voir le projet</a>
                <div class=\"progress\">
                  <div class=\"progress-bar progress-bar-success\" style=\"width: $pourcent\">$pourcent % collecté sur $sommeACollecter €</div>
                </div>

              </div>";

                $p2 = $p2 . " <li class=\"$active\">
                  <a href=\"#$idProjet\" data-toggle=\"tab\" aria-expanded=\"\">$nomCat</a>
                </li>";



                $active = "";
            }
        }
        $partenaire = Partenaire::all()->take(4);
        $partHTML = "";
        foreach($partenaire as $part){

            $partHTML = $partHTML . '
            <div class="text-center col-md-3">
                        <h2>' . $part->nom_partenaire . '</h2>

            <img src="' . $baseImages . $part->id_image . '.png"
            class="img-responsive">
          </div>';
        }

        $end = <<<END
              </div>
              <a class="left carousel-control" href="#carousel-example" data-slide="prev"><i class="icon-prev  fa fa-angle-left"></i></a>
              <a class="right carousel-control" href="#carousel-example" data-slide="next"><i class="icon-next fa fa-angle-right"></i></a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div id="myTabContent" class="tab-content">

              $p1
            </div>
          </div>

          <div class="col-md-4 well bs-component">
            <ol>
              <ul class="nav nav-pills nav-stacked">

                $p2

              </ul>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          $partHTML
        </div>
      </div>
    </div>


END;
        echo $front.$mid.$end;
        $this->footer();
    }

    public function afficherProjet($id){
        $this->header();

        $projet = Projet::find($id);
        if(!is_null($projet)){
            $baseImages = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/data/images/";

            $partenariat= Partenariat::where('id_projet', "=", $id)->take(4)->get();
            $partHTML = "";
            foreach($partenariat as $part1){

                $part = Partenaire::find($part1->id_partenaire);
                $partHTML = $partHTML . '
            <div class="text-center col-md-3">
                        <h2>' . $part->nom_partenaire . '</h2>

            <img src="' . $baseImages . $part->id_image . '.png"
            class="img-responsive">
          </div>';
            }


            $image = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/data/images/" . $projet->id_image . ".png";
            $pourcent = round($projet->somme_courente / $projet->somme_a_collecter * 100);
            $nom = $projet->nom_projet;
            if($pourcent > 100)
                $pourcentTaille = 100;
            else
                $pourcentTaille = $pourcent;
            $tmpRestant = (strtotime($projet->date_fin) - strtotime($projet->date_debut))/60/60/24;
            if($tmpRestant < 0){
                $tmpRestant = "Projet fini.";
            }else{
                $tmpRestant = round($tmpRestant) . " jours restants.";
            }
            $cadeau = Cadeau::where('id_projet', '=', $id)->orderBy('somme_a_atteindre', 'ASC')->get();
            $idCreateur = Creation::where('id_projet', '=', $id)->get();
            $createur = array();
            foreach($idCreateur as $crea){
                $createur[] = User::find($crea->id_user);
            }

            $financeur = Financement::where('id_projet', '=', $id)->get();
            $financeur = $financeur->toArray();
            $nbContrib = sizeof($financeur);
            $url = Slim::getInstance()->urlFor("financer");
            $urlAcc = Slim::getInstance()->urlFor("accueil");
            $modif = "";
            $title = "";
            $disabled = "";
            $creas = Creation::where("id_projet", "=", $id)->get();
            $myProj = false;
            if(!isset($_SESSION['user'])){
                $title = "Vous devez etre connecté pour financer un projet.";
                $disabled = "disabled";
            }else foreach($creas as $crea){
                if($crea->id_user == $_SESSION['user']->id_user)
                    $myProj = true ;
            }
            $varAdmin = false;
            if(isset($_SESSION['user']))
                $varAdmin = $_SESSION['user']->rules>0;
            if( $varAdmin || $myProj){
                $urlModif = str_replace(":id", $id, Slim::getInstance()->urlFor("changeProjet"));
                $modif = $modif."<li class=\"media\">
                            <div class=\"media-body\">
                              <a class='btn btn-primary' href='$urlModif'>Modifier</a>
                            </div>
                          </li>";
            }
            echo <<<END
<div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center">$nom</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-9">
            <img src="$image"
            class="img-responsive">
          </div>
          <div class="col-md-3">
            <ul class="media-list">
              <li class="media">
                <div class="media-body">
                  <h3 class="media-heading">$nbContrib</h3>
                  <p>contributeurs</p>
                </div>
              </li>
              <li class="media">
                <div class="media-body">
                  <h3 class="media-heading">$projet->somme_courente €</h3>
                  <p>sur $projet->somme_a_collecter €</p>
                  <div class="progress">
                    <div class="progress-bar" style="width: $pourcentTaille%;">$pourcent%</div>
                </div>
                </div>
              </li>
              <li class="media">
                <div class="media-body">
                  <h3 class="media-heading">$tmpRestant</h3>
                </div>
              </li>
              <li class="media">
END;

            if($tmpRestant>0)
            echo <<<END
                <form title="$title" method="get" class="form-horizontal" action=$url>
                  <div class="form-group">
                    <div class="col-sm-8">
                      <input type="number" class="form-control" name="somme" value="100" $disabled>
                      <input type="number" name="idProj" value="$id" hidden>
                    </div>
                    <button type="submit" class="btn btn-primary" $disabled>
                        Financer
                    </button>
                  </div>
                </form>
END;

            $descrProj = str_replace("../../data/images/", "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']). "/data/images/", $projet->description);
            $descrProj = str_replace("../data/images/", "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']). "/data/images/" , $descrProj);

            echo <<<END
              </li>
              $modif
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <p>$descrProj</p>
          </div>
          <div class="col-md-3">
            <ul class="media-list">
            <hr>
END;

            foreach($cadeau as $c){
                echo <<<END
              <li class="media">
                <div class="media-body">
                    <h3 class="media-heading">$c->nom_cadeau</h3>
                    <p>$c->description</p>
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>Contribution: $c->somme_a_atteindre € min</td>
                          <td>$c->nb_dispo disponibles</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
              </li>
              <hr>
END;
            }

            echo <<<END

            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
        <div class="col-md-12">    <hr><h3>En association avec :</h3></div>
          $partHTML
        </div>
      </div>
    </div>
END;
            $question =  Comentair::where("id_projet", "=", $id)->where("id_reponce", "=", "-1")->get();
            $this->question($question, $id);

        }else{
            $this->notFound("Projet non trouvable");
        }


        $this->footer();
    }

    public function notFound($err){
        $this->header();
        echo $err;
        $this->footer();
    }

    public function header(){

        $app = Slim::getInstance();
        $css = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/data/Bootstrap.css";
        $accueil = $app->urlFor("accueil");
        $connect = $app->urlFor("connection");
        $formCompte = $app->urlFor("formCompte");
        $search = $app->urlFor("recherche");
        $formPartenaire = $app->urlFor("creationPartenaire");
        $monCompt = $app->urlFor("nomCompt");
        $deco = $app->urlFor('deco');
        $jsall = str_replace("index.php", "", $accueil."data/js/all.js");
        $imgUrl = str_replace("index.php/", "", $accueil);

        echo <<<END
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="$jsall"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
    <link href="$css" rel="stylesheet" type="text/css">

  </head>

  <body>
    <div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="$accueil"><span>Site projet tut</span></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
        <form id="searchForm" method="get" action="$search" class="navbar-form navbar-right plop"
          role="search">
          <div id="searchBar" style="width:200px; display:none; z-index: 9999; overflow: hidden" class="input-group">
                <input type="text" style="padding:0px" name="sr" class="form-control" placeholder="Recherche">
         </div>
            <img id="imgSearch" src="$imgUrl/src/img/loupe.png" style="height:30px; margin-top:5px;"/>
        </form>
END;
        if(!isset($_SESSION['user'])){
            echo <<<END
        <img id="imgUsr" class="navbar-right" src="$imgUrl/src/img/user.png" style="height:30px; margin-top:5px;"/>
        <div id="divUsr" class="navbar-right" style="display:none; height:40px">
          <form method=get action=$formCompte class="navbar-form navbar-right">
            <button type="submit" class="btn btn-default">Créer un compte</button>
          </form>
          <form method=post action=$connect class="navbar-form navbar-right">
            <div class="form-group">
              <input name=pseudo type="text" class="form-control input-sm" placeholder="pseudo">
              <input name=pswd type="password" class="form-control input-sm" placeholder="mot de passe">
            </div>
            <button type="submit" class="btn btn-default">Se connecter</button>
          </form>
        </div>
          <ul class="nav navbar-left navbar-nav">


END;

        }else{
            $url = $app->urlFor("formProjet");
            echo <<<END
        <img id="imgUsr" class="navbar-right" src="$imgUrl/src/img/user.png" style="height:30px; margin-top:5px;"/>
        <div id="divUsr" class="navbar-right" style="display:none; width:400px; height:40px">
          <form method=post action=$deco class="navbar-form navbar-right plop" role="search">
            <button type="submit" class="btn btn-default">Se deconnecter</button>
          </form>
          <ul class="nav navbar-right navbar-nav">
            <li>
              <a href="$monCompt">Mon compte</a>
            </li>
          </ul>
        </div>
          <ul class="nav navbar-left navbar-nav">
            <li>
                <a href="$url">Créer un projet</a>
            </li>
            <li>
                <a href="$formPartenaire">Créer un partenaire</a>
            </li>
END;

        }
        $allProjet = str_replace(":id", 1, Slim::getInstance()->urlFor("allProjet"));
        echo <<<END
            <li class="dropdown">
              <a href="#" class="Categories" data-toggle="dropdown" role="button"
              aria-expanded="false">Projets</a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="$allProjet">Liste des projets</a>
                </li>
              <li class="divider"></li>
                <li>
                  Categories:


END;
        $cat = Categorie::all()->toArray();
        for($i = 0; $i < sizeof($cat); $i++ ){
            $nomCat = $cat[$i]['nom_categorie'];
            $url = str_replace(":id", $cat[$i]["id_categorie"], $app->urlFor("projectByCat"));
            $url = str_replace(":page", 1, $url);
            echo <<<END
                <li>
                  <a href="$url">$nomCat</a>
                </li>
END;
        }

        echo <<<END


                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
END;

    }

    public function footer(){
        echo <<<END
<div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
        <footer class="section">
      <div class="container">

        <div class="row">
          <div class="col-sm-6">
            <h1>Footer header</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisici elit,
              <br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
              <br>Ut enim ad minim veniam, quis nostrud</p>
          </div>
          <div class="col-sm-6">
            <p class="text-info text-right">
              <br>
              <br>
            </p>
            <div class="row">
              <div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 hidden-xs text-right">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
                <a href="#"><i class="fa fa-3x fa-facebook fa-fw"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
</body>

<script>init()</script>
</html>
END;

    }

    public function projetAll($selectedPage, $numberOfPages, $tabProj){
        $this->header();
        for($i = 0; $i < sizeof($tabProj); $i++){
            $image = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/data/images/".$tabProj[$i]['id_image'].".png";
            $descr = $tabProj[$i]['description'];
            $titre = $tabProj[$i]['nom_projet'];
            $urlProj = str_replace(":id", $tabProj[$i]["id_projet"] ,Slim::getInstance()->urlFor("projetId"));
            if($i % 2 == 0){

                echo <<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src=$image class="img-responsive">
          </div>
          <div class="col-md-6">
            <h1>$titre</h1>
            <div style="max-height: 500px; overflow: hidden">$descr</div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <a class="btn btn-primary" href="$urlProj">Voir le projet</a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
END;
            }else{
                echo<<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1>$titre</h1>
            <div style="max-height: 500px; overflow: hidden">$descr</div>
          </div>
          <div class="col-md-6">
            <img src=$image class="img-responsive">
          </div>
        </div>
      </div>
    </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <a class="btn btn-primary" href="$urlProj">Voir le projet</a>
          </div>
        </div>
      </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
END;
            }
        }

        echo <<<END


     <div class="container">
        <div class="row text-right">
          <div class="col-md-12">
            <ul class="pagination">
END;
        if($selectedPage>1){
            $prec = $selectedPage-1;
            $prec = str_replace(":id", $prec, Slim::getInstance()->urlFor("allProjet"));
            echo "<li>";
            echo "<a href=\"$prec\">Precedent</a>";
            echo "</li>";
        }

        for($i = 1; $i <=$numberOfPages; $i++){
            echo "<li ";
            if($i == $selectedPage)
                echo "class=\"active\">";
            else
                echo ">";
            $link = str_replace(":id", $i, Slim::getInstance()->urlFor("allProjet"));
            echo "<a href=\"$link\">$i</a>";
            echo "</li>";
        }

        if($selectedPage<$numberOfPages){
            $suiv = $selectedPage+1;
            $suiv = str_replace(":id", $suiv, Slim::getInstance()->urlFor("allProjet"));
            echo "<li>";
            echo "<a href=\"$suiv\">Suivant</a>";
            echo "</li>";
        }
        /*
              <li>
                <a href="#">Suivant</a>
              </li>
           */
        echo"</ul></div></div></div>";



        $this->footer();
    }

    public function projectByCat($selectedPage, $numberOfPages, $tabProj, $Cat)
    {
        $this->header();
        $Cat = $Cat->toArray();
        $nomCat = $Cat["nom_categorie"];
        echo <<<END
        <div class="section">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <h1>Catégorie: $nomCat</h1>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <hr>
              </div>
            </div>
          </div>
        </div>
END;

        for($i = 0; $i < sizeof($tabProj); $i++){
            $image = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/data/images/".$tabProj[$i]['id_image'].".png";
            $descr = str_replace("../../", "http://".$_SERVER['HTTP_HOST']."/",$tabProj[$i]['description']);
            $descr = str_replace("../", "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME'])."/",$descr);
            $titre = $tabProj[$i]['nom_projet'];
            $urlProj = str_replace(":id", $tabProj[$i]["id_projet"] ,Slim::getInstance()->urlFor("projetId"));

            if($i % 2 == 0){

                echo <<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src=$image class="img-responsive">
          </div>
          <div class="col-md-6">
            <h1>$titre</h1>
            <div style="max-height: 500px; overflow: hidden">$descr</div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <a class="btn btn-primary" href="$urlProj">Voir le projet</a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
END;
            }else{
                echo<<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1>$titre</h1>
            <div style="max-height: 500px; overflow: hidden">$descr</div>
          </div>
          <div class="col-md-6">
            <img src=$image class="img-responsive">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <a class="btn btn-primary" href="$urlProj">Voir le projet</a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
END;
            }
        }

        echo <<<END


     <div class="container">
        <div class="row text-right">
          <div class="col-md-12">
            <ul class="pagination">
END;
        if($selectedPage>1){
            $prec = $selectedPage-1;
            $prec = str_replace(":page", $prec, Slim::getInstance()->urlFor("projectByCat"));
            $prec = str_replace(":id", $Cat["id_categorie"], $prec);
            echo "<li>";
            echo "<a href=\"$prec\">Precedent</a>";
            echo "</li>";
        }

        for($i = 1; $i <=$numberOfPages; $i++){
            echo "<li ";
            if($i == $selectedPage)
                echo "class=\"active\">";
            else
                echo ">";
            $link = str_replace(":page", $i, Slim::getInstance()->urlFor("projectByCat"));
            $link = str_replace(":id", $Cat["id_categorie"], $link);

            echo "<a href=\"$link\">$i</a>";
            echo "</li>";
        }

        if($selectedPage<$numberOfPages){
            $suiv = $selectedPage+1;
            $suiv = str_replace(":page", $suiv, Slim::getInstance()->urlFor("projectByCat"));
            $suiv = str_replace(":id", $Cat["id_categorie"], $suiv);

            echo "<li>";
            echo "<a href=\"$suiv\">Suivant</a>";
            echo "</li>";
        }
        /*
              <li>
                <a href="#">Suivant</a>
              </li>
           */
        echo"</ul></div></div></div>";



        $this->footer();
    }

    public function profil($tabMesProj, $tabProjFi){

        $nom = $_SESSION['user']['nom'];
        $prenom = $_SESSION['user']['prenom'];
        $mail =  $_SESSION['user']['mail'];
        $pseudo = $_SESSION['user']['login'];
        $urlChangeMdp = Slim::getInstance()->urlFor("changeMdp");
        $urlChangeMail = Slim::getInstance()->urlFor("changeMail");


        $this->header();
        $vosProjet = "Vos projets";
        $vosFi ="Vos financements";
        if($_SESSION['user']->rules == 1){
            $vosProjet = "10 dernier projets";
            $vosFi = "Liste des Projets";
        }

        echo <<<END

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 well bs-component">
            <table class="table">
              <tbody>
                <tr>
                  <td style="width:180px">
                    <strong>Nom:</strong>
                  </td>
                  <td>$nom</td>
                  <td></td>
                </tr>
                <tr>
                  <td style="width:180px">
                    <strong>Prenom:</strong>
                  </td>
                  <td>$prenom</td>
                  <td></td>
                </tr>
                <tr>
                  <td style="width:180px">
                    <strong>Pseudo:</strong>
                  </td>
                  <td>$pseudo</td>
                  <td></td>
                </tr>
                <tr>
                  <td style="width:180px">
                    <strong>E-mail:</strong>
                  </td>
                  <td>$mail</td>
                  <td>
                    <a class="btn btn-primary btn-xs" href="$urlChangeMail">Changer votre e-mail</a>
                  </td>
                </tr>
                <tr>
                  <td style="width:180px">
                    <strong>Mot de pass:</strong>
                  </td>
                  <td>***********</td>
                  <td>
                    <a class="btn btn-primary btn-xs" href="$urlChangeMdp">Changer votre mot de passe</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-3 well bs-component">
            <h2>$vosProjet</h2>
            <ol>
              <ul class="nav nav-pills nav-stacked">
END;
        $li ="";
        $div = "";
        $active = "active";
        $height = sizeof($tabMesProj) * 40 + 60;
        foreach($tabMesProj as $projCour){
            $nomProj = $projCour['nom_projet'];
            $descr = str_replace("../../", "http://".$_SERVER['HTTP_HOST']."/",$projCour['description']);
            $descr = str_replace("../", "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME'])."/",$descr);
            $sommeACol = $projCour['somme_a_collecter'];
            $sommeCour = $projCour['somme_courente'];
            $id = $projCour['id_projet'];
            $url = str_replace(":id", $id, Slim::getInstance()->urlFor("projetId"));
            $url1 = str_replace(":id", $projCour['id_projet'], Slim::getInstance()->urlFor("changeProjet"));


            $li = $li . "<li class=\"$active in\">
                            <a href=\"#$id\" data-toggle=\"tab\" aria-expanded=\"\">$nomProj</a>
                         </li>";

            $div = $div . "<div class=\"tab-pane fade $active in \" id=\"$id\">
                                <div style=\"height: ".$height."px; min-height:300px; overflow: hidden;\">$descr</div>
                                <a class=\"btn btn-primary\" href=\"$url\">Voir le projet</a>
                                <a class=\"btn btn-primary\" href=\"$url1\">Modifier</a>
                                <p>$sommeCour € collectés sur $sommeACol €.
                           </div>";

            $active = "";
        }

        echo <<<END
$li
             </ul>
            </ol>
          </div>
          <div class="col-md-9">
            <div id="myTabContent" class="tab-content">
$div
            </div>
          </div>
        </div>
        <hr>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2>$vosFi</h2>
            <div style="height: 400px;">
            <style type="text/css">
                ::-webkit-scrollbar {
                  width: 8px;
                  height: 8px;
                }
                ::-webkit-scrollbar-button {
                  width: 0px;
                  height: 0px;
                }
                ::-webkit-scrollbar-thumb {
                  background: #df691a;
                  border: 0px none #ffffff;
                  border-radius: 50px;
                }
                ::-webkit-scrollbar-thumb:hover {
                  background: #b15315;
                }
                ::-webkit-scrollbar-thumb:active {
                  background: #b15315;
                }
                ::-webkit-scrollbar-track {
                  background: #666666;
                  border: 0px none #ffffff;
                  border-radius: 50px;
                }
                ::-webkit-scrollbar-track:hover {
                  background: #666666;
                }
                ::-webkit-scrollbar-track:active {
                  background: #333333;
                }
                ::-webkit-scrollbar-corner {
                  background: transparent;
                }
                </style>
END;
        if($_SESSION['user']->rules == 0) {
            echo <<<END
        <table class="table table-hover table-striped" style="float:left">
              <thead>
                <tr style="display: block; position: relative">
                  <th style = "width:200px">Nom du projet</th>
                  <th style = "width:150px">Date de financement</th>
                  <th style = "width:150px">Somme donnée</th>
                  <th style = "width:200px">Cadeau obtenu</th>
                  <th></th>
                </tr>
              </thead>
              <tbody style="overflow-y : scroll; max-height: 400px; display: block">
END;
            $tr = "";
            foreach ($tabProjFi as $projCour) {
                $dateFi = $projCour['dateFi'];
                $nomProj = $projCour['nom_projet'];
                $don = $projCour['don'];
                $url = str_replace(":id", $projCour['idProj'], Slim::getInstance()->urlFor("projetId"));
                $cadeau = "";
                if (isset($projCour["cadeau"]->nom_cadeau))
                    $cadeau = $projCour["cadeau"]->nom_cadeau . " (valeur: " . $projCour["cadeau"]->somme_a_atteindre . " €)";

                $tr = $tr . "<tr>
                          <td style = \"width:200px\">$nomProj</td>
                          <td style = \"width:150px\">$dateFi</td>
                          <td style = \"width:150px\">$don</td>
                          <td style = \"width:150px\">$cadeau</td>
                          <td style = \"width:100px\"><a class=\"btn btn-primary btn-xs\" href=\"$url\">Voir le projet</a></td>
                        </tr>";

            }

            echo <<<END

$tr
              </tbody>
            </table>
END;
        }else{
            echo <<<END
        <table class="table table-hover table-striped" style="float:left">
              <thead>
                <tr style="display: block; position: relative">
                  <th style = "width:200px">Nom du projet</th>
                  <th style = "width:150px">Date de création</th>
                  <th style = "width:150px">Date de fin</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody style="overflow-y : scroll; max-height: 400px; display: block">
END;
            $tr = "";
            foreach ($tabProjFi as $projCour) {
                $dateDebut = $projCour['date_debut'];
                $nomProj = $projCour['nom_projet'];
                $dateFin = $projCour['date_fin'];
                $url = str_replace(":id", $projCour['id_projet'], Slim::getInstance()->urlFor("projetId"));
                $url2 = str_replace(":id", $projCour['id_projet'], Slim::getInstance()->urlFor("changeProjet"));
                $idDel = $projCour['id_projet'];

                $tr = $tr . "<tr>
                          <td style = \"width:200px\">$nomProj</td>
                          <td style = \"width:150px\">$dateDebut</td>
                          <td style = \"width:150px\">$dateFin</td>
                          <td style = \"width:150px\"><a class=\"btn btn-primary btn-xs\" href=\"$url2\">modifier le projet</a></td>
                          <td style = \"width:150px\"><a class=\"btn btn-primary btn-xs\" onclick=\"delProj($idDel)\">suprimer le projet</a></td>
                          <td style = \"width:100px\"><a class=\"btn btn-primary btn-xs\" href=\"$url\">Voir le projet</a></td>
                        </tr>";

            }

            echo <<<END

$tr
              </tbody>
            </table>
END;
        }

        echo <<<END
            </div>
          </div>
        </div>
      </div>
    </div>

END;

        $this->footer();

    }

    private function reponse($c , $collectionRep){
        $compt = 1;
        echo <<<END
            <div id="" class="section">
              <div class="row">
                <div class="col-md-1">
                  <br>
                </div>
                <div class="col-md-11" style="background-color: rgba(100, 110, 120, 0.7);">
END;
        foreach($collectionRep as $rep){
            $id = "c".$c."r".$compt;
            $text = $rep->commentaire;
            echo <<<END
                  <h4 id="$id">Réponse $compt</h4>
                  <p>$text</p>
END;
            $compt++;
            $sousRep = Comentair::where("id_projet", "=", $rep->id_projet)->where("id_reponce", "=", $rep->id_commentair)->get();
            if(sizeof($sousRep->toArray()) > 0)
                $this->reponse($sousRep);

            echo"<hr>";

        }
        echo <<<END

                </div>
              </div>
            </div>
END;
    }

    private function question($collectionQuest, $id_projet){
        $compt = 1;
        $urlJS = str_replace("index.php/","",Slim::getInstance()->urlFor("accueil") . "data/js/Commentaire.js");

        echo <<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <script src="$urlJS"></script>
            <br><hr><h2>Commentaire</h2>
          </div>
        </div>
      </div>
    </div>

END;


        $urlRep = Slim::getInstance()->urlFor("addReponce");
        foreach($collectionQuest as $quest){
            $reponce = Comentair::where("id_projet", "=", $quest->id_projet)->where("id_reponce", "=", $quest->id_commentair)->get();
            $text = $quest->commentaire;
            $id_reponce = $quest->id_commentair;
            $id_comm = $quest->id_commentair;
            echo <<<END

    <div class="section">
      <div class="container">
        <br>
        <div class="row">
          <div class="col-md-12" style="background-color: rgba(80, 90, 100, 0.5);">
            <h4>Commentaire $compt</h4>
            <p>
              $text
              <br>
              <br>
            </p>
END;
            $num_rep = sizeof($reponce->toArray());
            if($num_rep > 0){
                $this->reponse($id_comm, $reponce);
            }

            echo <<<END
            <br>
            <a id="rep" class="btn btn-primary" onclick="javascript:formComment($id_comm)">Repondre</a>

            <div class="section formComm" id="com$id_comm" style="display:none">
              <div class="container">
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <form class="formReponse" method="post" action=$urlRep role="form">
                      <div class="form-group">
                        <textarea name="com" id="com" rows="10" cols="100" style="resize:none"></textarea>
                      </div>
                      <input hidden value="$id_projet" name="id_proj">
                      <input hidden value="$id_reponce" name="id_reponce">
                      <input hidden value="$num_rep" name="num_rep">
                      <button type="submit" class="btn btn-primary">Envoyer</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

END;
            $compt++;

        }
            $urlRep = Slim::getInstance()->urlFor("addQuestion");
        echo <<<END
<br>
    <div class="section">
      <div class="container">
        <div class="col-md-12 well">

          <a id="rep" class="btn btn-primary" onclick="javascript:formComment(-1)">Questionner</a>
            <div class="section formComm" id="com-1" style="display:none">
              <div class="container">
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <form class="formCommantaire" method="post" action=$urlRep role="form">
                      <div class="form-group">
                        <textarea name="com" id="com" rows="10" cols="100" style="resize:none"></textarea>
                      </div>
                      <input hidden value="$id_projet" name="id_proj">
                      <button type="submit" class="btn btn-primary">Envoyer</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

END;

    }

    public function serch($tabProj)
    {
        $this->header();
        for($i = 0; $i < sizeof($tabProj); $i++){
            $image = "http://" . $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']) . "/data/images/".$tabProj[$i]['id_image'].".png";
            $descr = $tabProj[$i]['description'];
            $titre = $tabProj[$i]['nom_projet'];
            $urlProj = str_replace(":id", $tabProj[$i]["id_projet"] ,Slim::getInstance()->urlFor("projetId"));
            if($i % 2 == 0){

                echo <<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src=$image class="img-responsive">
          </div>
          <div class="col-md-6">
            <h1>$titre</h1>
            <div style="max-height: 500px; overflow: hidden">$descr</div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <a class="btn btn-primary" href="$urlProj">Voir le projet</a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
END;
            }else{
                echo<<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1>$titre</h1>
            <div style="max-height: 500px; overflow: hidden">$descr</div>
          </div>
          <div class="col-md-6">
            <img src=$image class="img-responsive">
          </div>
        </div>
      </div>
    </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <a class="btn btn-primary" href="$urlProj">Voir le projet</a>
          </div>
        </div>
      </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
END;
            }
        }

        $this->footer();
    }
}