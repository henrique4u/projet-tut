<?php
/**
 * Created by PhpStorm.
 * User: BeniHenPCP
 * Date: 23/06/2015
 * Time: 08:50
 */

namespace vue;


use models\Categorie;
use models\Image;
use models\Partenaire;
use models\Projet;
use Slim\Slim;

class Form {

    public function formCadeau(){
        $app = Slim::getInstance();
        $nom = filter_var($app->request->post('nom'), FILTER_SANITIZE_STRING);
        $dateFin = filter_var($app->request->post('dateFin'));
        $Somme = filter_var($app->request->post('Somme'), FILTER_SANITIZE_NUMBER_FLOAT);
        $nbCadeau = filter_var($app->request->post('nbCadeau'), FILTER_SANITIZE_NUMBER_INT);
        $descr = $app->request->post('descr');
        $categ = filter_var($app->request->post('categ'), FILTER_SANITIZE_STRING);

        $vue = new render();
        $vue->header();

        $img = new Image();
        $img->save();
        if(move_uploaded_file($_FILES['img']['tmp_name'], "data/images/".$img->id_image . ".png"))

        $_SESSION["projetTmp"] = new Projet();
        $_SESSION["projetTmp"]->nom_projet = $nom;
        $_SESSION["projetTmp"]->date_debut = date("Y-m-d", time());
        $_SESSION["projetTmp"]->date_fin = $dateFin;
        $_SESSION["projetTmp"]->somme_a_collecter = $Somme;
        $_SESSION["projetTmp"]->somme_courente = 0;
        $_SESSION["projetTmp"]->id_categorie = $categ;
        $_SESSION["projetTmp"]->id_image = $img->id_image;
        $_SESSION["projetTmp"]->description = $descr;
        $_SESSION["post"] = $_POST;
        $url = $app->urlFor("addProjet");

        echo <<<END

   <div class="section " >
   <form method="post" action="$url" role="form">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div id="myTabContent" class="tab-content">
END;
        for($i = 1; $i<=$nbCadeau; $i++){
            $b = "";
            if($i == 1){
                $b = "active in";
            }
            $tinymce = str_replace("/index.php","",Slim::getInstance()->urlFor("accueil")."data/tinymce/tinymce.min.js");
            $fileManager = str_replace("/index.php","",Slim::getInstance()->urlFor("accueil")."data/filemanager");
            echo <<<END
                <div class="tab-pane fade $b" id="numCadeau$i">
                  <div class="form-group">
                    <label class="control-label label label-primary" for="nomCadeau$i">Nom du cadeau</label>
                    <input class="form-control" name="nomCadeau$i" id="nomCadeau$i" placeholder="Nom" type="text">
                  </div>
                  <script type="text/javascript" src="$tinymce"></script>
                  <script type="text/javascript">
                    tinymce.init({
                      resize: true,
                      skin: "charcoal",
                      language: "fr_FR",
                      selector: "textarea",
                      browser_spellcheck: true,
                        plugins: [
                             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                             "table contextmenu directionality emoticons paste textcolor responsivefilemanager",
                       ],
                       spellchecker_language: "fr_FR",
                       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
                       toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code",
                       image_advtab: true ,

                       external_filemanager_path:"$fileManager/",
                       filemanager_title:"Gestionnaire de fichiers" ,
                       external_plugins: { "filemanager" : "$fileManager/plugin.min.js"}
                    });
                  </script>
                  <br><br>
                  <div class="form-group">
                    <label class="control-label label label-primary" for="descr$i">Description</label>
                    <textarea class="form-control" rows="5" id="descr$i" name="descr$i"></textarea>
                  </div>
                  <div class="form-group">
                    <label class="control-label label label-primary" for="sommmeCadeau$i">Somme a atteindre</label>
                    <input class="form-control" name="sommmeCadeau$i" id="sommmeCadeau$i" value="0" min=0 type="number">
                  </div>
                  <div class="form-group">
                    <label class="control-label label label-primary" for="nbCadeau$i">Nombres disponible</label>
                    <input class="form-control" name="nbCadeau$i" id="nbCadeau$i" value="0" min=0 type="number">
                  </div>

              </div>

END;
        }
        echo <<<END

            </div>
          </div>
          <div class="col-md-4 well bs-component">
            <ol>
              <ul class="nav nav-pills nav-stacked">
END;
        for($i = 1; $i<=$nbCadeau; $i++){
            $b = false;
            $active = "";
            if($i == 1){
                $active = "class=\"active\"";
                $b = true;
            }
            echo <<<END
                <li $active>
                  <a href="#numCadeau$i" data-toggle="tab" aria-expanded="$b">Cadeau numero $i</a>
                </li>
END;

        }
        echo <<<END
                <li>
                <br>
                <acromym title="plop"><button type="submit" class="btn btn-primary">Créer votre projet</button></acronym>
                </li>
              </ul>
            </ol>
          </div>
        </div>
      </div>
      </form>
    </div>

END;


        $vue->footer();

    }

    public function addProjetForm(){
        $vue = new render();
        $vue->header();
        $url = Slim::getInstance()->urlFor("formProjetPart2");
        $tinymce = str_replace("/index.php","",Slim::getInstance()->urlFor("accueil")."data/tinymce/tinymce.min.js");
        $fileManager = str_replace("/index.php","",Slim::getInstance()->urlFor("accueil")."data/filemanager");

        $partenaire= Partenaire::all();
        $checkbox="";
        $compt="1";
        foreach($partenaire as $part){
            $checkbox = $checkbox.'
            <div class="form-group"><div class="col-sm-3"></div>

            <INPUT type="checkbox" id="check'.$compt.'" name="check'. $part->id_partenaire.'" value="'
                .$part->id_partenaire.'">
                <label for="check'.$compt.'">'.$part->nom_partenaire.'</label></div>';

            $compt++;
        }
        echo <<<END
        <div class="section">
        <form method="post" action=$url class="form-horizontal" role="form" enctype="multipart/form-data" xmlns="http://www.w3.org/1999/html">
      <div class="container">
        <div class="row">

            <div class="col-md-4 well bs-component"><br>
              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="nom" class="control-label">Nom de projet</label>
                </div>
                <div class="col-sm-9">
                  <input type="text" name="nom" class="form-control" id="nom" placeholder="Nom" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="dateFin" class="control-label">Date de fin</label>
                </div>
                <div class="col-sm-9">
                  <input type="date" name="dateFin" class="form-control" id="dateFin" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="Somme" class="control-label">Somme à collecter</label>
                </div>
                <div class="col-sm-9 ">
                  <input type="number" name="Somme" class="form-control" id="Somme" min=0 value=0 required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="nbCadeau" class="control-label">Nombre de cadeau</label>
                </div>
                <div class="col-sm-9">
                  <input type="number" name="nbCadeau" class="form-control" id="nbCadeau" min=1 max=4 value=3 required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="categ" class="control-label">Categorie</label>
                </div>
                <div class="col-sm-9">
                <select name="categ" id="categ" class="form-control" required>
                  <option disabled selected value="">Selectionez une categorie</option>


END;
        $catege = Categorie::all();
        foreach($catege as $c){
            echo"<option value='$c->id_categorie'>$c->nom_categorie</option>";
        }
        echo <<<END
                </select></div>
              </div>

                $checkbox

              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="img" class="control-label">Image du projet</label>
                </div>
                <div class="col-sm-9">
                  <input type="file" name="img" class="form-control" id="img" required>
                  <input type="hidden" name="MAX_FILE_SIZE" value="100000"/>
                </div>
              </div>

            </div>
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label label label-primary" for="descr">Description du projet</label>
                  <script type="text/javascript" src="$tinymce"></script>
                  <script type="text/javascript">
                    tinymce.init({
                      resize: false,
                      skin: "charcoal",
                      language: "fr_FR",
                      selector: "textarea",
                      browser_spellcheck: true,
                        plugins: [
                             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                             "table contextmenu directionality emoticons paste textcolor responsivefilemanager",
                       ],
                       spellchecker_language: "fr_FR",
                       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
                       toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code",
                       image_advtab: true ,

                       external_filemanager_path:"$fileManager/",
                       filemanager_title:"Gestionnaire de fichiers" ,
                       external_plugins: { "filemanager" : "$fileManager/plugin.min.js"}
                    });
                  </script>
                  <br><br>
                <textarea class="form-control" rows="30" id="descr" name="descr"></textarea>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tooltip on bottom">Suivant</button>
                </div>
              </div>
            </div>

        </div>
      </div>
      </form>
    </div>

END;

        $vue->footer();
    }

    public function addUserForm($noHeadFoot = false){

        if(!$noHeadFoot) {
            $vue = new render();
            $vue->header();
        }

        $url = Slim::getInstance()->urlFor("creerCompte");
        $urlCo = Slim::getInstance()->urlFor("connection");

        $err = "";
        $err2 = "";
        if(isset($_GET['err'])){
            if($_GET['err'] == 1){
                $err = "<div class=\"alert alert-dismissable alert-warning\">
                        <strong>Erreur. </strong>Un compte possède déjà ce login.</div>";
            }
            if($_GET['err'] == 3){
                $err = "<div class=\"alert alert-dismissable alert-warning\">
                        <strong>Erreur. </strong>Un compte possède déjà cet e-mail.</div>";
            }
            if($_GET['err'] == 2){
                $err2 = "<div class=\"alert alert-dismissable alert-warning\">
                        <strong>Erreur. </strong>Login ou mot de passe incorrect.</div>";
            }
        }

        echo <<<END
        <div class="section">
          <div class="container">
            <div class="row">
              <div class="col-md-12 well bs-component">
              <form class="form-horizontal" role="form" action="$urlCo" method="post">
              $err2
                <h3>Se connecter</h3>
                <hr>
                <div class="form-group">
                    <div class="col-sm-2">
                      <label for="login" class="control-label">Login</label>
                    </div>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="login" placeholder="Login" name="pseudo" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <label for="pswd" class="control-label">Password</label>
                    </div>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="pswd" placeholder="PassWord" name="pswd" required>
                    </div>
                  </div>
                  <hr>
                  <button type="submit" class="btn btn-primary center-block" data-toggle="tooltip" data-placement="bottom" title="Valider" data-original-title="Tooltip on bottom">Valider</button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
        <br><br>

        <div class="section">
          <div class="container">
            <div class="row">
              <div class="col-md-12 well bs-component">
<form class="form-horizontal" role="form" action="$url" method="post">
        <div class="">
          <div class="">
            <div class="">
              <div class="">

$err
              <h3>Créer un compte.</h3>
                <hr>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <label for="nom" class="control-label">Nom</label>
                    </div>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nom" placeholder="Nom" name="nom" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <label for="prenom" class="control-label">Prenom</label>
                    </div>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="prenom" placeholder="Prenom" name="prenom" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <label for="email" class="control-label">Email</label>
                    </div>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <label for="dateNaiss" class="control-label">Date de naissance</label>
                    </div>
                    <div class="col-sm-10">
                      <input type="date" class="form-control" id="dateNaiss" placeholder="dateNaiss" name="dateNaiss" required>
                    </div>
                  </div>

              </div>
            </div>
          </div>
        </div>
        <div class="">
          <div class="">
            <div class="">
              <div class="">
                <hr>

                  <div class="form-group">
                    <div class="col-sm-2">
                      <label for="newlogin" class="control-label">Login</label>
                    </div>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="newlogin" placeholder="Login" name="newlogin" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <label for="newpswd" class="control-label">Password</label>
                    </div>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="newpswd" placeholder="PassWord" name="newpswd" required>
                    </div>
                  </div>

              </div>
            </div>
          </div>
        </div>
        <div class="">
          <div class="">
            <div class="">
              <div class="">
                <hr>
              </div>
            </div>
          </div>
        </div>
        <div class="section">
          <div class="container">
            <div class="row">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Valider" data-original-title="Tooltip on bottom">Valider</button>
              </div>
            </div>
          </div>
        </div>
    </form>
    </div> </div> </div> </div>
END;
        if(!$noHeadFoot) {
            $vue->footer();
        }

    }

    public function changeMdp(){
        $render = new render();
        $render->header();
        $urlJS = str_replace("index.php/","",Slim::getInstance()->urlFor("accueil") . "/data/js/VerifPasswd.js");
        $urlAct = Slim::getInstance()->urlFor("actionChangMdp");
        $alert = "";
        $success = "";
        if(isset($_GET['err'])) {
            if($_GET['err'] == 1)
                $alert = "<div class=\"alert alert-dismissable alert-warning\">
                            <strong>Oupsss</strong> Une erreur est survenue.</div>";
            if($_GET['err'] == 2)
                $alert = "<div class=\"alert alert-dismissable alert-warning\">
                            Mauvais mod de passe.</div>";
        }
        if(isset($_GET['success'])) {
            $success = "<div class=\"alert alert-dismissable alert-success\">
                            Mot de passe changée.</div>";
        }
        echo <<<END

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 well bs-component">
          <p>Cette page vous permet de changer votre mot de passe</p>
          $alert
          $success
            <form role="form" method="post" action="$urlAct">
              <div class="form-group">
                <label class="control-label" for="oldPasswd">Mot de passe actuel</label>
                <input class="form-control" id="oldPasswd" name="oldPasswd"
                placeholder="************" type="password">
              </div>
              <div id="divNewPasswd" class="form-group">
                <label class="control-label" for="newPasswd">Nouveau mot de passe</label>
                <input class="form-control " id="newPasswd" name="newPasswd"
                placeholder="" type="password">
              </div>
              <div id="divVerifPasswd" class="form-group">
                <label class="control-label" for="verifNewPasswd">Verification du mod de passe</label>
                <input class="form-control" id="verifNewPasswd" name="verifNewPasswd"
                placeholder="" type="password">
              </div>
              <button id="changePswd" type="submit" class="btn btn-primary center-block" disabled>Changer le mod de passe</button>
            </form>
            <script src="$urlJS"></script>
            <script>verifChangeMdp()</script>
          </div>
        </div>
      </div>
    </div>

END;

        $render->footer();

    }

    public function changeMail(){
        $render = new render();
        $render->header();

        $url = Slim::getInstance()->urlFor("actionChangeMail");
        $msg = "";
        if(isset($_GET['err'])){
            $msg = "<div class=\"alert alert-dismissable alert-warning\">
                            Mauvais mod de passe.</div>";;
        }
        if(isset($_GET['success'])){
            $msg = "<div class=\"alert alert-dismissable alert-success\">
                            E-mail changé.</div>";;
        }

        echo <<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 well bs-component">
          $msg
            <form role="form" method="post" action="$url">
              <div class="form-group">
                <label class="control-label" for="mail">Entrer un nouveau e-maill</label>
                <input class="form-control" id="mail" name="mail"
                placeholder="exemple@email.com" type="email">
              </div>
              <div class="form-group">
                <label class="control-label" for="pswd">Par securitée, enter votre mot de passe</label>
                <input class="form-control"
                id="pswd" name="pswd" placeholder="***********" type="password">
              </div>
              <button type="submit" class="btn btn-primary center-block">Changer e-mail</button>
            </form>
          </div>
        </div>
      </div>
    </div>
END;

        $render->footer();
    }

    public function creerPartenaire(){
        $render = new render();
        $render->header();

        $url = Slim::getInstance()->urlFor("actionCreationPartenaire");

        if(isset($_GET['err'])){
            $msg = "<div class=\"alert alert-dismissable alert-warning\">
                            Ce nom de partenaire existe deja.</div>";
            echo $msg;
        }

        echo <<<END
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 well bs-component">
            <form role="form" method="post" action="$url" enctype="multipart/form-data" xmlns="http://www.w3.org/1999/html">
              <div class="form-group">
                <label class="control-label" for="nom_partenaire">Entrer un nom d'entreprise</label>
                <input class="form-control" id="nom_partenaire" name="nom_partenaire"
                placeholder="Nom de l'entreprise" type="text">
              </div>
              <div class="form-group">
                <label class="control-label" for="email_partenaire">Entrer l'e-mail de l'entreprise</label>
                <input class="form-control"
                id="email_partenaire" name="email_partenaire" placeholder="example@email.com" type="email">
              </div>
              <div class="form-group">
                <label class="control-label" for="adresse_partenaire">Entrer l'adresse de l'entreprise</label>
                <input class="form-control"
                id="adresse_partenaire" name="adresse_partenaire" placeholder="Adresse de l'entreprise" type="text">
              </div>
              <div class="form-group">
                  <label for="logo_partenaire" class="control-label">Logo de l'entreprise</label>
                  <input type="file" name="logo_partenaire" class="form-control" id="logo_partenaire" required>
                  <input type="hidden" name="MAX_FILE_SIZE" value="100000"/>
              </div>
              <button type="submit" class="btn btn-primary center-block">Creer entreprise</button>
            </form>
          </div>
        </div>
      </div>
    </div>
END;

        $render->footer();
    }

    public function changeProj($projet)
    {
        $vue = new render();
        $vue->header();
        $url = str_replace(":id", $projet->id_projet,Slim::getInstance()->urlFor("changeProjetAct"));
        $tinymce = str_replace("/index.php","",Slim::getInstance()->urlFor("accueil")."data/tinymce/tinymce.min.js");
        $fileManager = str_replace("/index.php","",Slim::getInstance()->urlFor("accueil")."data/filemanager");

        $idProj = $projet->id_projet;
        $nom = $projet->nom_projet;
        $descr = $projet->description;
        $sommeACollecter = $projet->somme_a_collecter;
        $dateFi = $projet->date_fin;

        echo <<<END
        <div class="section">
        <form method="post" action=$url class="form-horizontal" role="form" enctype="multipart/form-data" xmlns="http://www.w3.org/1999/html">
      <div class="container">
        <div class="row">

            <div class="col-md-4 well bs-component"><br>
              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="nom" class="control-label">Nom de projet</label>
                </div>
                <div class="col-sm-9">
                  <input type="text" name="nom" class="form-control" id="nom" value="$nom" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="Somme" class="control-label">Somme à collecter</label>
                </div>
                <div class="col-sm-9 ">
                  <input type="number" name="Somme" class="form-control" id="Somme" min=0 value=$sommeACollecter required>
                  <input type="hidden" name="id_projet" class="form-control" value="$idProj">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3 text-right">
                  <label for="dateFi" class="control-label">Date de fin</label>
                </div>
                <div class="col-sm-9 ">
                  <input type="date" name="dateFi" class="form-control" id="dateFi" min=0 value=$dateFi required>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label label label-primary" for="descr">Description du projet</label>
                  <script type="text/javascript" src="$tinymce"></script>
                  <script type="text/javascript">
                    tinymce.init({
                      resize: false,
                      skin: "charcoal",
                      language: "fr_FR",
                      selector: "textarea",
                      browser_spellcheck: true,
                        plugins: [
                             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                             "table contextmenu directionality emoticons paste textcolor responsivefilemanager",
                       ],
                       spellchecker_language: "fr_FR",
                       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
                       toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code",
                       image_advtab: true ,

                       external_filemanager_path:"$fileManager/",
                       filemanager_title:"Gestionnaire de fichiers" ,
                       external_plugins: { "filemanager" : "$fileManager/plugin.min.js"}
                    });
                  </script>
                  <br><br>
                <textarea class="form-control" rows="30" id="descr" name="descr">$descr</textarea>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tooltip on bottom">Valider les changements</button>
                </div>
              </div>
            </div>
        </div>
      </div>
      </form>
    </div>

END;

        $vue->footer();
    }
}