-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 02 Novembre 2015 à 21:47
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projet-tut`
--

-- --------------------------------------------------------

--
-- Structure de la table `cadeau`
--

CREATE TABLE IF NOT EXISTS `cadeau` (
  `id_cadeau` int(11) NOT NULL AUTO_INCREMENT,
  `nom_cadeau` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `somme_a_atteindre` int(11) NOT NULL,
  `nb_dispo` int(11) NOT NULL,
  `id_projet` int(11) NOT NULL,
  `id_image` int(11) NOT NULL,
  PRIMARY KEY (`id_cadeau`),
  KEY `id_projet` (`id_projet`),
  KEY `id_image` (`id_image`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Contenu de la table `cadeau`
--

INSERT INTO `cadeau` (`id_cadeau`, `nom_cadeau`, `description`, `somme_a_atteindre`, `nb_dispo`, `id_projet`, `id_image`) VALUES
(1, 'cadeau 1', 'zqesrdfghjkl', 100, 4, 2, 1),
(2, 'cadeau 2', 'zqesrdfghjkl', 50, 4, 2, 1),
(3, 'srsrgg', '<p>rgesgrg</p>', 111, 1111, 3, 0),
(4, '1212', '<p>1212</p>', 12, 1212, 3, 0),
(5, '1212', '<p>121212</p>', 1212, 12121210, 3, 0),
(6, 'fgdfgdfg', '<p>dfgdwfgdfgf wgd gd fdfgw gf dw</p>', 4444, 4444, 4, 0),
(7, 'hgdhfghf', '<p>ghfgdhfgdjghjghjbgjghj</p>', 1111, 1111111, 11, 0),
(8, 'dsfsdffg', '<p>ghfgdfhfgdhfgdh</p>', 4444, 45454545, 12, 0),
(9, '543453453', '<p>455343543</p>', 453453, 453453453, 12, 0),
(10, '453453543', '<p>453453453</p>', 54345345, 3453453, 12, 0),
(11, 'g', '<p>gg</p>', 1, 1, 13, 0),
(12, '1212', '<p>12121</p>', 21212, 121212, 13, 0),
(13, '1212', '<p>1212</p>', 12121, 21212, 13, 0),
(14, 'dfsdfs', '<p>dfsdfsdfsdf</p>', 44, 444444, 14, 0),
(15, 'gfhfgh', '<p>fdghfdghf</p>', 1, 1089, 15, 0),
(16, 'fdgfg', '<p>dfgdfgdfg</p>', 444, 44444, 16, 0),
(17, 'fghfghfg', '<p>hfghfghfgh</p>', 445, 454545, 17, 0),
(18, '454545', '<p>454545454</p>', 545, 2147483645, 17, 0),
(19, '4565867', '<p>45654</p>', 45456, 54, 17, 0),
(20, 'dsfsf', '<p>sdfsdfd</p>', 545, 54, 18, 0),
(24, 'sdsd', '<p>sdfgffgd</p>', 4, 4, 20, 0),
(25, '4242', '<p>424</p>', 24242, 4242, 20, 0),
(26, '42', '<p>424</p>', 4242, 4242, 20, 0),
(27, 'zerzer', '<p>zerzerzer</p>', 45454, 454545, 21, 0),
(28, 'sdfgdfgsdgfg', '<p>sdfgdsgesrg</p>', 555, 442, 22, 0),
(29, 'gjhgjhgjh', '<p>HGHGJHGJH</p>', 150, 118, 23, 0),
(30, 'dfgdfg', '<p>dfgdfg</p>', 14, 23, 24, 0),
(31, 'Cadeau 1', '<p>auris congue pharetra facilisis. Aenean commodo porttitor consectetur. Nulla feugiat, ipsum ornare lobortis pretium, eros nulla hendrerit ligula, vitae rutrum lorem felis ac erat. Proin magna tellus, consectetur sit amet iaculis sit amet, fringilla vitae justo. Donec non dui at magna lobortis faucibus eget vitae dolor. Vestibulum sit amet blandit augue. Curabitur sed molestie lacus, ac vehicula dolor. Nam dolor lorem, lacinia vel ex nec, semper blandit urna. Duis ut nulla egestas, egestas urna sit amet, sodales augue.</p>\r\n<p>Nullam felis quam, laoreet sit amet iaculis a, aliquam quis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris id metus faucibus, porttitor ligula vitae, ornare arcu. Quisque pulvinar ultrices mi, id rhoncus lectus bibendum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a massa arcu. In vel dapibus nisi. Donec blandit rhoncus felis, nec rutrum dui volutpat ut. Vestibulum vehicula luctus bibendum. In pharetra tellus ac ex fermentum aliquam. Maecenas molestie cursus orci vestibulum efficitur.</p>', 90, 50, 25, 0),
(32, 'dsfgf', '<p>auris congue pharetra facilisis. Aenean commodo porttitor consectetur. Nulla feugiat, ipsum ornare lobortis pretium, eros nulla hendrerit ligula, vitae rutrum lorem felis ac erat. Proin magna tellus, consectetur sit amet iaculis sit amet, fringilla vitae justo. Donec non dui at magna lobortis faucibus eget vitae dolor. Vestibulum sit amet blandit augue. Curabitur sed molestie lacus, ac vehicula dolor. Nam dolor lorem, lacinia vel ex nec, semper blandit urna. Duis ut nulla egestas, egestas urna sit amet, sodales augue.</p>\r\n<p>Nullam felis quam, laoreet sit amet iaculis a, aliquam quis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris id metus faucibus, porttitor ligula vitae, ornare arcu. Quisque pulvinar ultrices mi, id rhoncus lectus bibendum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a massa arcu. In vel dapibus nisi. Donec blandit rhoncus felis, nec rutrum dui volutpat ut. Vestibulum vehicula luctus bibendum. In pharetra tellus ac ex fermentum aliquam. Maecenas molestie cursus orci vestibulum efficitur.</p>', 55, 2221, 25, 0),
(33, 'Cadeau 3', '<p>auris congue pharetra facilisis. Aenean commodo porttitor consectetur. Nulla feugiat, ipsum ornare lobortis pretium, eros nulla hendrerit ligula, vitae rutrum lorem felis ac erat. Proin magna tellus, consectetur sit amet iaculis sit amet, fringilla vitae justo. Donec non dui at magna lobortis faucibus eget vitae dolor. Vestibulum sit amet blandit augue. Curabitur sed molestie lacus, ac vehicula dolor. Nam dolor lorem, lacinia vel ex nec, semper blandit urna. Duis ut nulla egestas, egestas urna sit amet, sodales augue.</p>\r\n<p>Nullam felis quam, laoreet sit amet iaculis a, aliquam quis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris id metus faucibus, porttitor ligula vitae, ornare arcu. Quisque pulvinar ultrices mi, id rhoncus lectus bibendum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a massa arcu. In vel dapibus nisi. Donec blandit rhoncus felis, nec rutrum dui volutpat ut. Vestibulum vehicula luctus bibendum. In pharetra tellus ac ex fermentum aliquam. Maecenas molestie cursus orci vestibulum efficitur.</p>', 5, 1000, 25, 0),
(34, 'Cadeau 4', '<p>auris congue pharetra facilisis. Aenean commodo porttitor consectetur. Nulla feugiat, ipsum ornare lobortis pretium, eros nulla hendrerit ligula, vitae rutrum lorem felis ac erat. Proin magna tellus, consectetur sit amet iaculis sit amet, fringilla vitae justo. Donec non dui at magna lobortis faucibus eget vitae dolor. Vestibulum sit amet blandit augue. Curabitur sed molestie lacus, ac vehicula dolor. Nam dolor lorem, lacinia vel ex nec, semper blandit urna. Duis ut nulla egestas, egestas urna sit amet, sodales augue.</p>\r\n<p>Nullam felis quam, laoreet sit amet iaculis a, aliquam quis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris id metus faucibus, porttitor ligula vitae, ornare arcu. Quisque pulvinar ultrices mi, id rhoncus lectus bibendum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a massa arcu. In vel dapibus nisi. Donec blandit rhoncus felis, nec rutrum dui volutpat ut. Vestibulum vehicula luctus bibendum. In pharetra tellus ac ex fermentum aliquam. Maecenas molestie cursus orci vestibulum efficitur.</p>', 2000, 50, 25, 0);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `nom_categorie` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `nom_categorie`) VALUES
(1, 'test'),
(2, 'test2');

-- --------------------------------------------------------

--
-- Structure de la table `commentair`
--

CREATE TABLE IF NOT EXISTS `commentair` (
  `id_commentair` int(11) NOT NULL AUTO_INCREMENT,
  `id_projet` int(11) NOT NULL,
  `id_reponce` int(11) NOT NULL,
  `commentaire` text NOT NULL,
  `stage` int(2) NOT NULL,
  `num_rep` int(11) NOT NULL,
  PRIMARY KEY (`id_commentair`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Contenu de la table `commentair`
--

INSERT INTO `commentair` (`id_commentair`, `id_projet`, `id_reponce`, `commentaire`, `stage`, `num_rep`) VALUES
(1, 22, -1, 'Question 1', 0, -1),
(2, 22, 1, 'Reponce 1', 1, 0),
(3, 22, 1, 'reponce 2', 1, 1),
(4, 22, 3, 'reponce 2.1', 2, 0),
(5, 22, 1, 'reponce 3', 1, 2),
(49, 24, -1, 'tempor. Integer ac tortor quis lacus rutrum molestie id vel est. Proin condimentum pulvinar lacus, eget tincidunt arcu semper vel. Aliquam erat volutpat. Donec id tortor consectetur, semper orci eu, rhoncus nibh. Cras dictum elementum tellus, non vehicula justo lacinia quis. Aliquam felis lorem, hendrerit non elementum vitae, mattis in tellus.', 0, -1),
(50, 24, 49, 'entum sem, eu suscipit mauris. Praesent facilisis nec nisl eget maximus. Etiam sem ipsum, consectetur ac risus et, viverra vestibulum elit. Cras molestie metus ut tellus dapibus, id facilisis elit bibendum. Nam tristique enim vel urna ullamcorper dictum. Pellentesque habitant m', 1, 0),
(51, 24, 49, 'entum sem, eu suscipit mauris. Praesent facilisis nec nisl eget maximus. Etiam sem ipsum, consectetur ac risus et, viverra vestibulum elit. Cras molestie metus ut tellus dapibus, id facilisis elit bibendum. Nam tristique enim vel urna ullamcorper dictum. Pellentesque habitant m', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `creation`
--

CREATE TABLE IF NOT EXISTS `creation` (
  `id_user` int(11) NOT NULL,
  `id_projet` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_projet`),
  KEY `id_user` (`id_user`),
  KEY `id_projet` (`id_projet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `creation`
--

INSERT INTO `creation` (`id_user`, `id_projet`) VALUES
(1, 2),
(2, 2),
(4, 15),
(4, 16),
(4, 17),
(4, 18),
(4, 20),
(4, 21),
(4, 22),
(5, 23),
(11, 24),
(13, 25);

-- --------------------------------------------------------

--
-- Structure de la table `financement`
--

CREATE TABLE IF NOT EXISTS `financement` (
  `id_fi` int(11) NOT NULL AUTO_INCREMENT,
  `somme_donnee` int(11) NOT NULL,
  `date_fi` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_projet` int(11) NOT NULL,
  `cadeau_obtenu` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id_fi`),
  KEY `id_user` (`id_user`),
  KEY `id_projet` (`id_projet`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=214 ;

--
-- Contenu de la table `financement`
--

INSERT INTO `financement` (`id_fi`, `somme_donnee`, `date_fi`, `id_user`, `id_projet`, `cadeau_obtenu`) VALUES
(1, 80, '2015-09-27', 3, 2, -1),
(2, 80, '2015-09-27', 3, 2, -1),
(3, 100, '2015-10-06', 4, 2, -1),
(4, 100, '2015-10-06', 4, 2, -1),
(5, 100, '2015-10-06', 4, 8, -1),
(6, 100, '2015-10-06', 4, 8, -1),
(7, 100, '2015-10-06', 4, 8, -1),
(8, 100, '2015-10-07', 4, 13, -1),
(9, 100, '2015-10-07', 4, 13, -1),
(10, 100, '2015-10-07', 4, 13, -1),
(11, 100, '2015-10-07', 4, 13, -1),
(12, 100, '2015-10-07', 4, 13, -1),
(13, 100, '2015-10-07', 4, 13, -1),
(14, 100, '2015-10-07', 4, 13, -1),
(15, 100, '2015-10-07', 4, 13, -1),
(16, 100, '2015-10-07', 4, 13, -1),
(17, 100, '2015-10-07', 4, 13, -1),
(18, 100, '2015-10-07', 4, 13, -1),
(19, 100, '2015-10-07', 4, 13, -1),
(20, 100, '2015-10-07', 4, 13, -1),
(21, 100, '2015-10-07', 4, 13, -1),
(22, 100, '2015-10-07', 4, 13, -1),
(23, 100, '2015-10-07', 4, 13, -1),
(24, 8, '2015-10-07', 4, 2, -1),
(25, 100, '2015-10-07', 4, 16, -1),
(26, 100, '2015-10-07', 4, 16, -1),
(27, 989898, '2015-10-07', 4, 16, -1),
(28, 545465465, '2015-10-07', 4, 16, -1),
(29, 50, '2015-10-08', 4, 2, -1),
(30, 100, '2015-10-08', 4, 2, -1),
(31, 100, '2015-10-08', 4, 2, -1),
(32, 100, '2015-10-08', 4, 2, -1),
(33, 100, '2015-10-08', 4, 2, -1),
(34, 100, '2015-10-08', 4, 2, -1),
(35, 100, '2015-10-08', 4, 2, -1),
(36, 100, '2015-10-08', 4, 2, -1),
(37, 100, '2015-10-08', 4, 2, -1),
(38, 100, '2015-10-08', 4, 2, -1),
(39, 100, '2015-10-08', 4, 2, -1),
(40, 100, '2015-10-08', 4, 2, -1),
(41, 100, '2015-10-08', 4, 2, -1),
(42, 100, '2015-10-08', 4, 2, -1),
(43, 100, '2015-10-08', 4, 2, -1),
(44, 100, '2015-10-08', 4, 2, -1),
(45, 100, '2015-10-08', 4, 2, -1),
(46, 100, '2015-10-08', 4, 2, -1),
(47, 100, '2015-10-08', 4, 2, -1),
(48, 100, '2015-10-08', 4, 2, -1),
(49, 100, '2015-10-08', 4, 2, -1),
(50, 100, '2015-10-08', 4, 2, -1),
(51, 100, '2015-10-08', 4, 2, -1),
(52, 100, '2015-10-08', 4, 2, -1),
(53, 100, '2015-10-08', 4, 2, -1),
(54, 100, '2015-10-08', 4, 2, -1),
(55, 100, '2015-10-08', 4, 2, -1),
(56, 100, '2015-10-08', 4, 2, -1),
(57, 100, '2015-10-08', 4, 2, -1),
(58, 100, '2015-10-08', 4, 2, -1),
(59, 100, '2015-10-08', 4, 2, -1),
(60, 100, '2015-10-08', 4, 2, -1),
(61, 100, '2015-10-08', 4, 2, -1),
(62, 100, '2015-10-08', 4, 2, -1),
(63, 100, '2015-10-08', 4, 2, -1),
(64, 100, '2015-10-08', 4, 2, -1),
(65, 100, '2015-10-08', 4, 2, -1),
(66, 100, '2015-10-08', 4, 2, -1),
(67, 100, '2015-10-08', 4, 2, -1),
(68, 100, '2015-10-08', 4, 2, -1),
(69, 100, '2015-10-08', 4, 2, -1),
(70, 100, '2015-10-08', 4, 2, -1),
(71, 100, '2015-10-08', 4, 2, -1),
(72, 100, '2015-10-08', 4, 2, -1),
(73, 100, '2015-10-08', 4, 2, -1),
(74, 100, '2015-10-08', 4, 2, -1),
(75, 100, '2015-10-08', 4, 2, -1),
(76, 100, '2015-10-08', 4, 2, -1),
(77, 100, '2015-10-08', 4, 2, -1),
(78, 100, '2015-10-08', 4, 2, -1),
(79, 100, '2015-10-08', 4, 2, -1),
(80, 100, '2015-10-08', 4, 2, -1),
(81, 100, '2015-10-08', 4, 2, -1),
(82, 100, '2015-10-08', 4, 2, -1),
(83, 100, '2015-10-08', 4, 2, -1),
(84, 100, '2015-10-08', 4, 2, -1),
(85, 100, '2015-10-08', 4, 2, -1),
(86, 100, '2015-10-08', 4, 2, -1),
(87, 100, '2015-10-08', 4, 2, -1),
(88, 100, '2015-10-08', 4, 2, -1),
(89, 100, '2015-10-08', 4, 2, -1),
(90, 100, '2015-10-08', 4, 2, -1),
(91, 100, '2015-10-08', 4, 2, -1),
(92, 100, '2015-10-08', 4, 2, -1),
(93, 100, '2015-10-08', 4, 2, -1),
(94, 100, '2015-10-08', 4, 2, -1),
(95, 100, '2015-10-08', 4, 2, -1),
(96, 100, '2015-10-08', 4, 2, -1),
(97, 100, '2015-10-08', 4, 2, -1),
(98, 100, '2015-10-08', 4, 2, -1),
(99, 100, '2015-10-08', 4, 2, -1),
(100, 100, '2015-10-08', 4, 2, -1),
(101, 100, '2015-10-08', 4, 2, -1),
(102, 100, '2015-10-08', 4, 2, -1),
(103, 100, '2015-10-08', 4, 2, -1),
(104, 100, '2015-10-08', 4, 2, -1),
(105, 100, '2015-10-08', 4, 2, -1),
(106, 100, '2015-10-08', 4, 2, -1),
(107, 100, '2015-10-08', 4, 2, -1),
(108, 100, '2015-10-08', 4, 2, -1),
(109, 100, '2015-10-08', 4, 2, -1),
(110, 100, '2015-10-08', 4, 2, -1),
(111, 100, '2015-10-08', 4, 2, -1),
(112, 100, '2015-10-08', 4, 2, -1),
(113, 100, '2015-10-08', 4, 2, -1),
(114, 100, '2015-10-08', 4, 2, -1),
(115, 100, '2015-10-08', 4, 2, -1),
(116, 100, '2015-10-08', 4, 2, -1),
(117, 100, '2015-10-08', 4, 2, -1),
(118, 100, '2015-10-08', 4, 2, -1),
(119, 100, '2015-10-08', 4, 2, -1),
(120, 100, '2015-10-08', 4, 2, -1),
(121, 100, '2015-10-08', 4, 2, -1),
(122, 100, '2015-10-08', 4, 2, -1),
(123, 100, '2015-10-08', 4, 2, -1),
(124, 100, '2015-10-08', 4, 2, -1),
(125, 100, '2015-10-08', 4, 2, -1),
(126, 100, '2015-10-08', 4, 2, -1),
(127, 100, '2015-10-08', 4, 2, -1),
(128, 100, '2015-10-08', 4, 2, -1),
(129, 100, '2015-10-08', 4, 2, -1),
(130, 100, '2015-10-08', 4, 2, -1),
(131, 100, '2015-10-08', 4, 2, -1),
(132, 100, '2015-10-08', 4, 2, -1),
(133, 100, '2015-10-08', 4, 2, -1),
(134, 100, '2015-10-08', 4, 2, -1),
(135, 100, '2015-10-08', 4, 2, -1),
(136, 100, '2015-10-08', 4, 2, -1),
(137, 100, '2015-10-08', 4, 2, -1),
(138, 100, '2015-10-08', 4, 2, -1),
(139, 100, '2015-10-08', 4, 2, -1),
(140, 100, '2015-10-08', 4, 2, -1),
(141, 100, '2015-10-08', 4, 2, -1),
(142, 100, '2015-10-08', 4, 2, -1),
(143, 100, '2015-10-08', 4, 2, -1),
(144, 100, '2015-10-08', 4, 2, -1),
(145, 100, '2015-10-08', 4, 2, -1),
(146, 100, '2015-10-08', 4, 15, -1),
(147, 100, '2015-10-08', 4, 15, -1),
(148, 100, '2015-10-08', 4, 15, -1),
(149, 100, '2015-10-08', 4, 15, -1),
(150, 100, '2015-10-08', 4, 15, -1),
(151, 100, '2015-10-08', 4, 15, -1),
(152, 100, '2015-10-08', 4, 15, -1),
(153, 100, '2015-10-08', 4, 15, -1),
(154, 100, '2015-10-08', 4, 15, -1),
(155, 100, '2015-10-08', 4, 15, -1),
(156, 100, '2015-10-08', 4, 15, -1),
(157, 100, '2015-10-08', 4, 15, 15),
(158, 100, '2015-10-08', 4, 15, 15),
(159, 100, '2015-10-08', 4, 15, 15),
(160, 100, '2015-10-08', 4, 15, 15),
(161, 100, '2015-10-08', 4, 15, 15),
(162, 100, '2015-10-08', 4, 15, 15),
(163, 100, '2015-10-08', 4, 15, 15),
(164, 100, '2015-10-08', 4, 15, 15),
(165, 100, '2015-10-08', 4, 15, 15),
(166, 100, '2015-10-08', 4, 15, 15),
(167, 100, '2015-10-08', 4, 15, 15),
(168, 100, '2015-10-08', 4, 15, 15),
(169, 100, '2015-10-08', 4, 15, 15),
(170, 100, '2015-10-08', 4, 15, 15),
(171, 100, '2015-10-08', 4, 15, 15),
(172, 100, '2015-10-08', 4, 15, 15),
(173, 100, '2015-10-08', 4, 15, 15),
(174, 100, '2015-10-08', 4, 15, 15),
(175, 100, '2015-10-08', 4, 15, 15),
(176, 100, '2015-10-08', 4, 17, -1),
(177, 600, '2015-10-08', 4, 17, 18),
(178, 600, '2015-10-08', 4, 17, 18),
(179, 100, '2015-10-08', 4, 18, -1),
(180, 100, '2015-10-08', 4, 18, -1),
(181, 999999, '2015-10-08', 4, 18, 20),
(182, 100, '2015-10-08', 4, 17, -1),
(183, 100, '2015-10-08', 4, 17, -1),
(184, 100, '2015-10-08', 4, 17, -1),
(185, 100, '2015-10-08', 4, 17, -1),
(186, 100, '2015-10-08', 4, 17, -1),
(187, 99999, '2015-10-08', 4, 17, 19),
(199, 100, '2015-10-10', 4, 22, -1),
(200, 600, '2015-10-10', 4, 22, 28),
(201, 8000, '2015-10-11', 4, 22, 28),
(202, 10000, '2015-10-14', 4, 3, 5),
(203, 200, '2015-10-20', 5, 23, 29),
(204, 200, '2015-10-20', 5, 23, 29),
(205, 250, '2015-10-20', 5, 23, 29),
(206, 100, '2015-10-20', 5, 23, -1),
(207, 1300, '2015-10-21', 5, 3, 5),
(208, 100, '2015-11-02', 11, 15, 15),
(209, 100, '2015-11-02', 11, 15, 15),
(210, 100, '2015-11-02', 11, 15, 15),
(211, 100, '2015-11-02', 11, 24, 30),
(212, 100, '2015-11-02', 11, 24, 30),
(213, 100, '2015-11-02', 11, 24, 30);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_image`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id_image`) VALUES
(1),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32);

-- --------------------------------------------------------

--
-- Structure de la table `partenaire`
--

CREATE TABLE IF NOT EXISTS `partenaire` (
  `id_partenaire` int(11) NOT NULL AUTO_INCREMENT,
  `nom_partenaire` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mail_partenaire` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adresse_partenaire` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_image` int(11) NOT NULL,
  PRIMARY KEY (`id_partenaire`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `partenaire`
--

INSERT INTO `partenaire` (`id_partenaire`, `nom_partenaire`, `mail_partenaire`, `adresse_partenaire`, `id_image`) VALUES
(3, 'Batman corp', 'Batou@bat.man', 'gotham city', 29),
(4, 'Anon', 'Anon@imous.gouv', 'nope', 30),
(5, 'Superman corp', 'klark@kent.super', 'petiteVille', 31),
(6, 'tigrou', 'ti@grou.savane', 'savane', 32);

-- --------------------------------------------------------

--
-- Structure de la table `partenariat`
--

CREATE TABLE IF NOT EXISTS `partenariat` (
  `id_projet` int(11) NOT NULL,
  `id_partenaire` int(11) NOT NULL,
  PRIMARY KEY (`id_projet`,`id_partenaire`),
  KEY `id_projet` (`id_projet`),
  KEY `id_partenaire` (`id_partenaire`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE IF NOT EXISTS `projet` (
  `id_projet` int(11) NOT NULL AUTO_INCREMENT,
  `nom_projet` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `somme_a_collecter` int(11) NOT NULL,
  `somme_courente` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `id_image` int(11) NOT NULL,
  PRIMARY KEY (`id_projet`),
  KEY `id_categorie` (`id_categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `projet`
--

INSERT INTO `projet` (`id_projet`, `nom_projet`, `date_debut`, `date_fin`, `description`, `somme_a_collecter`, `somme_courente`, `id_categorie`, `id_image`) VALUES
(2, 'test', '2015-06-22', '2015-06-27', 'description a 2 balles', 1080, 12458, 1, 1),
(3, 'rerg', '2015-08-28', '1111-11-11', '<p>fgfjgfhjg</p>\r\n<p>ffnghcngchn</p>\r\n<p><img src="../../projet-tut/data/images/city_attack_by_kvacm-d4m42g9.jpg" alt="city_attack_by_kvacm-d4m42g9" width=400px/></p>', 1111111, 11300, 2, 1),
(4, 'prj3', '2015-09-15', '0000-00-00', '<p>yolo plop</p>\r\n<p>truc a 2 balles</p>\r\n<p><img src="../data/images/1.png" alt="1" width="155" height="116" /></p>\r\n<p>&nbsp;</p>\r\n<p>fgnbfgnfgnfgngfnfgnxngfgnxfgn fnjxf nfxn fg xfg nfxnxfgnxfng</p>', 999999999, 0, 1, 1),
(8, 'projet 7', '2015-09-09', '2015-09-30', 'zeqxtxezqtxtxqzetxzeqtxzetxvqceqczzqtxqe', 111, 322, 2, 1),
(11, 'fdgdfg', '2015-09-27', '0011-11-11', '<p>dfghdhdghgfhdh</p>', 2121231351, 0, 2, 1),
(12, 'sddfgfdhdfhgfh', '2015-10-07', '0016-04-04', '<p><img src="../data/images/city_attack_by_kvacm-d4m42g9_1.jpg" alt="city_attack_by_kvacm-d4m42g9_1" width="525" height="295" /></p>\r\n<p>fgojdfgbdsihjgb</p>\r\n<p>sdgfhgh</p>\r\n<p>fghfgdh</p>\r\n<p>dfghfgdhfgh</p>', 999999999, 0, 2, 1),
(13, 'test', '2015-10-07', '0044-04-04', '<p>&lt;h2&gt;plop&lt;/h2&gt;</p>', 444, 1600, 1, 1),
(14, 'mksfgnmosfdhgmoq', '2015-10-07', '0001-12-12', '<p>dfdfgdfgdfgdfgf</p>', 212121, 0, 1, 1),
(15, 'sdqdf', '2015-10-07', '2017-06-15', '<p>modif le 30/10/2015</p>\r\n<p>5454ds64f65sdf465sd4f56sd65f4d6f4sdf</p>\r\n<p>sqfqfdsfsdf</p>\r\n<p>scscqscqsc</p>', 10000, 3300, 1, 1),
(16, 'patte a la mort', '2015-10-07', '2222-02-21', '<p>dfsdfsdfsdfsdfsdfsdff</p>', 2121221545, 546455563, 2, 1),
(17, 'san semper. Sed et', '2015-10-08', '2016-08-28', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mollis odio sit amet iaculis suscipit. Sed sit amet dui quis metus dictum ullamcorper. Etiam efficitur justo id leo ultricies, eu suscipit neque laoreet. Integer vitae imperdiet dui. Duis eget efficitur erat. Aenean libero velit, iaculis et lorem ut, aliquam maximus purus. Duis metus lacus, interdum at libero quis, bibendum facilisis leo. Duis in fermentum sem, eu suscipit mauris. Praesent facilisis nec nisl eget maximus. Etiam sem ipsum, consectetur ac risus et, viverra vestibulum elit. Cras molestie metus ut tellus dapibus, id facilisis elit bibendum. Nam tristique enim vel urna ullamcorper dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris congue sem erat, non semper lacus consectetur at. Aliquam blandit vel mi nec tempor.</p>\r\n<p>Mauris congue pharetra facilisis. Aenean commodo porttitor consectetur. Nulla feugiat, ipsum ornare lobortis pretium, eros nulla hendrerit ligula, vitae rutrum lorem felis ac erat. Proin magna tellus, consectetur sit amet iaculis sit amet, fringilla vitae justo. Donec non dui at magna lobortis faucibus eget vitae dolor. Vestibulum sit amet blandit augue. Curabitur sed molestie lacus, ac vehicula dolor. Nam dolor lorem, lacinia vel ex nec, semper blandit urna. Duis ut nulla egestas, egestas urna sit amet, sodales augue.</p>\r\n<p>Nullam felis quam, laoreet sit amet iaculis a, aliquam quis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris id metus faucibus, porttitor ligula vitae, ornare arcu. Quisque pulvinar ultrices mi, id rhoncus lectus bibendum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a massa arcu. In vel dapibus nisi. Donec blandit rhoncus felis, nec rutrum dui volutpat ut. Vestibulum vehicula luctus bibendum. In pharetra tellus ac ex fermentum aliquam. Maecenas molestie cursus orci vestibulum efficitur.</p>\r\n<p>Suspendisse eleifend eleifend mauris, at consequat erat gravida eu. Sed gravida lacus non mauris iaculis, a consequat sem congue. Cras sed tortor lectus. Fusce nec libero ultricies, luctus urna a, commodo purus. In hac habitasse platea dictumst. Cras pulvinar pharetra tempor. Integer ac tortor quis lacus rutrum molestie id vel est. Proin condimentum pulvinar lacus, eget tincidunt arcu semper vel. Aliquam erat volutpat. Donec id tortor consectetur, semper orci eu, rhoncus nibh. Cras dictum elementum tellus, non vehicula justo lacinia quis. Aliquam felis lorem, hendrerit non elementum vitae, mattis in tellus.</p>\r\n<p>In a augue non dui imperdiet egestas. Etiam congue nulla in purus accumsan imperdiet. Aenean accumsan, ligula dignissim ullamcorper elementum, lectus nisi mattis felis, nec fringilla ante dolor sollicitudin ipsum. Vivamus molestie est nisl, non eleifend urna porta at. Quisque convallis sapien ex, rhoncus luctus quam pulvinar sed. Duis rutrum sollicitudin mi. Cras euismod lectus a blandit viverra. Nullam in enim eget sem luctus laoreet in at odio. Quisque id venenatis urna, sed euismod ante.</p>\r\n<p>Quisque placerat interdum nulla, non lobortis mi vehicula at. Nunc auctor est malesuada tortor pellentesque, at luctus justo sodales. Proin orci lorem, tincidunt vitae malesuada eu, molestie vel massa. Sed vulputate semper consequat. Etiam venenatis, leo fermentum pulvinar dictum, lorem tellus malesuada tellus, sed maximus justo nisi quis sem. Fusce dignissim a nisl ut facilisis. Donec in ultricies arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nulla velit, aliquam vel dignissim vel, placerat a massa. Praesent consectetur viverra lacus non accumsan. Quisque eget turpis efficitur augue blandit imperdiet. Nulla elementum urna eget sem hendrerit auctor. Donec eget lacus velit. In dictum, lectus eu mollis gravida, ipsum leo viverra tortor, eget gravida dolor eros ac ligula.</p>\r\n<p>Suspendisse posuere volutpat lobortis. Nam ac turpis arcu. Ut facilisis diam in leo maximus, sed feugiat augue mattis. Aliquam luctus faucibus suscipit. Aenean ultricies nisi vitae odio sollicitudin elementum. Aliquam erat volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent in ex luctus, volutpat turpis nec, eleifend quam. Aenean elit nunc, hendrerit quis justo id, pulvinar suscipit lacus. Sed bibendum elementum urna, quis ullamcorper augue dignissim et. Pellentesque aliquet tellus id eros pretium, ac scelerisque massa posuere. Integer mattis erat vitae eros mattis, id consectetur ipsum gravida. Nullam enim lacus, porta in ligula ut, rutrum condimentum augue. Curabitur ac felis pulvinar, viverra est non, condimentum odio. Pellentesque vitae neque in risus auctor dapibus id a elit. Aenean ultricies sit amet sem eget ullamcorper.</p>\r\n<table style="height: 361px;" width="558">\r\n<tbody>\r\n<tr>\r\n<td>fdfdf</td>\r\n<td>sdfsdf</td>\r\n<td>dsfsdf</td>\r\n<td>sdfdsf</td>\r\n<td>sdfsdf</td>\r\n<td>sdfsdfsdf</td>\r\n<td>sdfsdfsdfsdf</td>\r\n</tr>\r\n<tr>\r\n<td>dsfsdfsdf</td>\r\n<td>ez</td>\r\n<td>eze</td>\r\n<td>ze</td>\r\n<td>z</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n</tr>\r\n<tr>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>zez</td>\r\n</tr>\r\n<tr>\r\n<td>e</td>\r\n<td>ze</td>\r\n<td>zeze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>zee</td>\r\n</tr>\r\n<tr>\r\n<td>zzez</td>\r\n<td>e</td>\r\n<td>zez</td>\r\n<td>&nbsp;</td>\r\n<td>z</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n</tr>\r\n<tr>\r\n<td>ze</td>\r\n<td>e</td>\r\n<td>e</td>\r\n<td>&nbsp;</td>\r\n<td>zee</td>\r\n<td>z</td>\r\n<td>ze</td>\r\n</tr>\r\n<tr>\r\n<td>ze</td>\r\n<td>eze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n<td>ze</td>\r\n</tr>\r\n<tr>\r\n<td>ze</td>\r\n<td>ezezaefs</td>\r\n<td>efsdf</td>\r\n<td>fz</td>\r\n<td>zf</td>\r\n<td>zff</td>\r\n<td>zf</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>fjdsfhsouhdfiuhsdg</p>', 4454545, 101799, 2, 1),
(18, ' dui at magna l', '2015-10-08', '2015-10-08', '<p>auris congue pharetra facilisis. Aenean commodo porttitor consectetur. Nulla feugiat, ipsum ornare lobortis pretium, eros nulla hendrerit ligula, vitae rutrum lorem felis ac erat. Proin magna tellus, consectetur sit amet iaculis sit amet, fringilla vitae justo. Donec non dui at magna lobortis faucibus eget vitae dolor. Vestibulum sit amet blandit augue. Curabitur sed molestie lacus, ac vehicula dolor. Nam dolor lorem, lacinia vel ex nec, semper blandit urna. Duis ut nulla egestas, egestas urna sit amet, sodales augue.</p>\r\n<p>Nullam felis quam, laoreet sit amet iaculis a, aliquam quis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris id metus faucibus, porttitor ligula vitae, ornare arcu. Quisque pulvinar ultrices mi, id rhoncus lectus bibendum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a massa arcu. In vel dapibus nisi. Donec blandit rhoncus felis, nec rutrum dui volutpat ut. Vestibulum vehicula luctus bibendum. In pharetra tellus ac ex fermentum aliquam. Maecenas molestie cursus orci vestibulum efficitur.</p>\r\n<p>Suspendisse eleifend eleifend mauris, at consequat erat gravida eu. Sed gravida lacus non mauris iaculis, a consequat sem congue. Cras sed tortor lectus. Fusce nec libero ultricies, luctus urna a, commodo purus. In hac habitasse platea dictumst. Cras pulvinar pharetra tempor. Integer ac tortor quis lacus rutrum molestie id vel est. Proin condimentum pulvinar lacus, eget tincidunt arcu semper vel. Aliquam erat volutpat. Donec id tortor consectetur, semper orci eu, rhoncus nibh. Cras dictum elementum tellus, non vehicula justo lacinia quis. Aliquam felis lorem, hendrerit non elementum vitae, mattis in tellus.</p>\r\n<p>In a augue non dui imperdiet egestas. Etiam congue nulla in purus accumsan imperdiet. Aenean accumsan, ligula dignissim ullamcorper elementum, lectus nisi mattis felis, nec fringilla ante dolor sollicitudin ipsum. Vivamus molestie est nisl, non eleifend urna porta at. Quisque convallis sapien ex, rhoncus luctus quam pulvinar sed. Duis rutrum sollicitudin mi. Cras euismod lectus a blandit viverra. Nullam in enim eget sem luctus laoreet in at odio. Quisque id venenatis urna, sed euismod ante.</p>\r\n<p>Quisque placerat interdum nulla, non lobortis mi vehicula at. Nunc auctor est malesuada tortor pellentesque, at luctus justo sodales. Proin orci lorem, tincidunt vitae malesuada eu, molestie vel massa. Sed vulputate semper consequat. Etiam venenatis, leo fermentum pulvinar dictum, lorem tellus malesuada tellus, sed maximus justo nisi quis sem. Fusce dignissim a nisl ut facilisis. Donec in ultricies arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nulla velit, aliquam vel dignissim vel, placerat a massa. Praesent consectetur viverra lacus non accumsan. Quisque eget turpis efficitur augue blandit imperdiet. Nulla elementum urna eget sem hendrerit auctor. Donec eget lacus velit. In dictum, lectus eu mollis gravida, ipsum leo viverra tortor, eget gravida dolor eros ac ligula.</p>\r\n<p>Suspendisse posuere volutpat lobortis. Nam ac turpis arcu. Ut facilisis diam in leo maximus, sed feugiat augue mattis. Aliquam luctus faucibus suscipit. Aenean ultricies nisi vitae odio sollicitudin elementum. Aliquam erat volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent in ex luctus, volutpat turpis nec, eleifend quam. Aenean elit nunc, hendrerit quis justo id, pulvinar suscipit lacus. Sed bibendum elementum urna, quis ullamcorper augue dignissim et. Pellentesque aliquet tellus id eros pretium, ac scelerisque massa posuere. Integer mattis erat vitae eros mattis, id consectetur ipsum gravida. Nullam enim lacus, porta in ligula ut, rutrum condimentum augue. Curabitur ac felis pulvinar, viverra est non, condimentum odio. Pellentesque vitae neque in risus auctor dapibus id a elit. Aenean ultricies sit amet sem eget ullamcorper.</p>', 212000, 1000199, 1, 1),
(20, 'eros ac ligula.', '2015-10-10', '0444-04-04', '<div id="lipsum">\r\n<p>Quisque placerat interdum nulla, non lobortis mi vehicula at. Nunc auctor est malesuada tortor pellentesque, at luctus justo sodales. Proin orci lorem, tincidunt vitae malesuada eu, molestie vel massa. Sed vulputate semper consequat. Etiam venenatis, leo fermentum pulvinar dictum, lorem tellus malesuada tellus, sed maximus justo nisi quis sem. Fusce dignissim a nisl ut facilisis. Donec in ultricies arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nulla velit, aliquam vel dignissim vel, placerat a massa. Praesent consectetur viverra lacus non accumsan. Quisque eget turpis efficitur augue blandit imperdiet. Nulla elementum urna eget sem hendrerit auctor. Donec eget lacus velit. In dictum, lectus eu mollis gravida, ipsum leo viverra tortor, eget gravida dolor eros ac ligula.</p>\r\n<p>Suspendisse posuere volutpat lobortis. Nam ac turpis arcu. Ut facilisis diam in leo maximus, sed feugiat augue mattis. Aliquam luctus faucibus suscipit. Aenean ultricies nisi vitae odio sollicitudin elementum. Aliquam erat volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent in ex luctus, volutpat turpis nec, eleifend quam. Aenean elit nunc, hendrerit quis justo id, pulvinar suscipit lacus. Sed bibendum elementum urna, quis ullamcorper augue dignissim et. Pellentesque aliquet tellus id eros pretium, ac scelerisque massa posuere. Integer mattis erat vitae eros mattis, id consectetur ipsum gravida. Nullam enim lacus, porta in ligula ut, rutrum condimentum augue. Curabitur ac felis pulvinar, viverra est non, condimentum odio. Pellentesque vitae neque in risus auctor dapibus id a elit. Aenean ultricies sit amet sem eget ullamcorper.</p>\r\n<p>Vestibulum vel aliquet nulla, quis gravida risus. Fusce malesuada consectetur aliquet. Integer sollicitudin neque sit amet pretium ornare. Donec in ipsum sit amet mauris pulvinar aliquam sed fermentum nulla. Proin tristique mi quis odio placerat eleifend. Cras tristique purus quis rutrum commodo. Vivamus hendrerit lectus vitae ultricies dignissim. Nam semper in dui sit amet varius.</p>\r\n<p>Pellentesque vel sapien at nulla tempor egestas non quis dolor. In vestibulum hendrerit rutrum. Nunc quis blandit ante. Nulla ut tincidunt turpis. Nulla vulputate arcu nulla, in volutpat diam blandit at. Sed semper lorem massa, in lobortis sapien ornare eu. Nullam eu ante justo. Nullam tincidunt felis sed lobortis lobortis. Curabitur mollis neque ac leo posuere interdum. Vivamus scelerisque, magna ac dapibus euismod, sem dolor dictum massa, nec dictum nulla diam eget dolor. Donec placerat gravida viverra. Nam nec pharetra libero.</p>\r\n<p>Sed lectus tortor, finibus quis consequat dapibus, condimentum in magna. Nunc egestas eros eget dolor tincidunt maximus. Vestibulum malesuada orci ante, ut semper dolor pharetra nec. Nunc turpis leo, facilisis pretium mi vitae, egestas rutrum nulla. Vivamus lectus risus, dignissim et ante ac, fringilla consectetur lectus. Sed bibendum imperdiet dictum. Aliquam sit amet massa vulputate, accumsan leo ut, pulvinar justo. Vestibulum mollis auctor augue, in vulputate justo aliquet in. Proin consectetur sem finibus magna tincidunt gravida. Etiam porttitor mollis risus vel ultricies. Curabitur ut nulla ligula. Ut urna sem, mattis vitae arcu a, vulputate scelerisque lorem. Duis ut quam sed nibh consequat elementum non non mi. Morbi ornare sem sed arcu condimentum blandit</p>\r\n</div>\r\n<div id="generated">&nbsp;</div>', 10500, 0, 1, 8),
(21, 'teeros ac ligdfula.', '2015-10-10', '5455-04-05', '<p>Nullam felis quam, laoreet sit amet iaculis a, aliquam quis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris id metus faucibus, porttitor ligula vitae, ornare arcu. Quisque pulvinar ultrices mi, id rhoncus lectus bibendum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a massa arcu. In vel dapibus nisi. Donec blandit rhoncus felis, nec rutrum dui volutpat ut. Vestibulum vehicula luctus bibendum. In pharetra tellus ac ex fermentum aliquam. Maecenas molestie cursus orci vestibulum efficitur.</p>\r\n<p>Suspendisse eleifend eleifend mauris, at consequat erat gravida eu. Sed gravida lacus non mauris iaculis, a consequat sem congue. Cras sed tortor lectus. Fusce nec libero ultricies, luctus urna a, commodo purus. In hac habitasse platea dictumst. Cras pulvinar pharetra tempor. Integer ac tortor quis lacus rutrum molestie id vel est. Proin condimentum pulvinar lacus, eget tincidunt arcu semper vel. Aliquam erat volutpat. Donec id tortor consectetur, semper orci eu, rhoncus nibh. Cras dictum elementum tellus, non vehicula justo lacinia quis. Aliquam felis lorem, hendrerit non elementum vitae, mattis in tellus.</p>\r\n<p>In a augue non dui imperdiet egestas. Etiam congue nulla in purus accumsan imperdiet. Aenean accumsan, ligula dignissim ullamcorper elementum, lectus nisi mattis felis, nec fringilla ante dolor sollicitudin ipsum. Vivamus molestie est nisl, non eleifend urna porta at. Quisque convallis sapien ex, rhoncus luctus quam pulvinar sed. Duis rutrum sollicitudin mi. Cras euismod lectus a blandit viverra. Nullam in enim eget sem luctus laoreet in at odio. Quisque id venenatis urna, sed euismod ante.</p>\r\n<p>Quisque placerat interdum nulla, non lobortis mi vehicula at. Nunc auctor est malesuada tortor pellentesque, at luctus justo sodales. Proin orci lorem, tincidunt vitae malesuada eu, molestie vel massa. Sed vulputate semper consequat. Etiam venenatis, leo fermentum pulvinar dictum, lorem tellus malesuada tellus, sed maximus justo nisi quis sem. Fusce dignissim a nisl ut facilisis. Donec in ultricies arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nulla velit, aliquam vel dignissim vel, placerat a massa. Praesent consectetur viverra lacus non accumsan. Quisque eget turpis efficitur augue blandit imperdiet. Nulla elementum urna eget sem hendrerit auctor. Donec eget lacus velit. In dictum, lectus eu mollis gravida, ipsum leo viverra tortor, eget gravida dolor eros ac ligula.</p>', 44444, 0, 2, 9),
(22, 'san semper. Sed et', '2015-10-10', '2020-11-01', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at tempus orci. Curabitur tempor, sem at finibus commodo, purus velit auctor lacus, id consectetur felis tellus vitae quam. Donec nec tortor ut lacus ullamcorper lacinia.</p>\r\n<p>&nbsp;</p>\r\n<p><img src="../data/images/city_attack_by_kvacm-d4m42g9_1.jpg" alt="city_attack_by_kvacm-d4m42g9_1" width="661" height="372" /></p>\r\n<p>Aliquam iaculis posuere dolor, vel sollicitudin augue varius id. Vestibulum scelerisque urna sed gravida scelerisque. Nulla feugiat id nunc a bibendum. Suspendisse consectetur erat ultrices, maximus nisl vitae, ullamcorper felis. Vestibulum mattis justo ut metus lobortis, sed mollis leo interdum. In bibendum sem vitae nisi mattis, vitae blandit lectus tempor. Donec sollicitudin sapien vitae nibh scelerisque, ac tristique dolor porta. Pellentesque finibus urna tellus, vel venenatis orci varius ac. Nunc non nunc et sapien laoreet sodales eu et lectus. Curabitur rhoncus efficitur leo quis sagittis.</p>\r\n<p>&nbsp;</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n</tr>\r\n<tr>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n</tr>\r\n<tr>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque augue sit amet tellus lacinia tincidunt. Donec gravida sapien non accumsan semper. Sed et erat tempus, dignissim eros consequat, accumsan felis. Morbi faucibus ligula a sem rhoncus auctor. Mauris hendrerit felis quis lobortis lobortis. Cras at temp</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Morbi ornare vel neque at finibus. Etiam a dapibus urna, eget laoreet turpis. Cras nec elementum lacus, sit amet varius nunc. Donec laoreet odio sit amet libero tincidunt, et rhoncus justo iaculis. Nam mattis velit ut nisl sodales, sit amet lacinia diam malesuada. Aliquam sed placerat est. Sed varius dui vel ante bibendum, at elementum lorem tempor. Phasellus orci mauris, accumsan eu urna at, finibus faucibus arcu. Sed at lectus in nulla consequat sodales sed vel est. Sed malesuada congue mi ac eleifend. Nam auctor massa est, non tristique odio vestibulum id. Ut dignissim nisi sem, nec viverra tellus finibus in. In vitae metus sem. Quisque gravida lacus nulla, et fermentum sem maximus at.</p>\r\n<p>Fusce scelerisque accumsan porta. Phasellus nec facilisis leo. Duis ornare erat vitae mauris tristique, in malesuada est consectetur. Nullam feugiat vestibulum est at mattis. Duis at sem sem. Nulla erat massa, maximus id lacus ut, convallis suscipit risus. In hac habitasse platea dictumst. Vivamus ante dolor, lacinia vitae luctus in, malesuada sit amet metus. Cras sed volutpat diam. Nulla eget mauris lectus.</p>\r\n<p>Duis iaculis massa vitae iaculis pulvinar. Etiam ut consectetur tellus, vel ultricies nulla. Morbi non leo quis nunc luctus mattis. Morbi diam ex, imperdiet eu arcu eget, feugiat semper velit. Nam ullamcorper gravida diam, vel interdum dui scelerisque non. In volutpat dignissim lorem non blandit. Sed vel arcu sagittis, vehicula risus nec, vulputate nisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed tempor in nisi tempus consequat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur augue ipsum, blandit in mattis ut, tristique a nisi. Ut non vestibulum nunc. Praesent at pretium erat, eu sagittis quam. Vivamus ut posuere tellus. Ut sapien nisl, lacinia vel turpis interdum, fringilla facilisis enim.</p>\r\n<p>&nbsp;</p>\r\n<p><img src="../../data/images/24.png" alt="" width="345" height="240" /></p>', 190040, 8700, 2, 21),
(23, 'vitae sagittis nisl', '2015-10-20', '2015-10-08', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sagittis nec justo quis ultricies. Integer bibendum sapien sed nisi ultrices facilisis. Pellentesque quis nibh tortor. Aenean at nisl at magna accumsan tristique. Mauris ac mi vestibulum, malesuada ipsum id, aliquam arcu. Vestibulum imperdiet mauris egestas nulla auctor, eget rhoncus metus efficitur. Praesent dapibus, ante id elementum porta, lectus neque pulvinar erat, et auctor erat dui id magna.</p>\r\n<p>Aliquam ut dui molestie lectus viverra vestibulum. Donec convallis porta est sed ultrices. Quisque vel volutpat purus. Ut magna risus, posuere in iaculis et, efficitur vel nulla. Proin condimentum blandit tellus, ac tincidunt libero porta pharetra. Maecenas id metus ac mi vulputate dictum. Vestibulum lobortis, lorem ut feugiat laoreet, mi erat mollis eros, eu pellentesque sem lectus id quam. Nulla accumsan ex vel sodales maximus. Aliquam ut orci enim. Cras et ante ac massa cursus pretium sodales vitae enim. Quisque sem urna, imperdiet vitae tincidunt a, placerat at sapien. Sed sed blandit odio, et imperdiet ligula. Suspendisse commodo sagittis auctor. Maecenas nec purus vitae risus lacinia bibendum. Quisque porttitor tellus eget nulla suscipit viverra. Morbi felis augue, mollis nec lectus a, sollicitudin tincidunt sapien.</p>\r\n<p>Aenean a augue at lectus malesuada pharetra nec in tellus. Suspendisse nec tempor est, sed consectetur odio. Nunc gravida non libero sit amet vehicula. Aenean quis risus faucibus, cursus quam id, pretium lorem. Praesent molestie pulvinar purus non tincidunt. Fusce facilisis maximus leo, eget hendrerit leo pulvinar ac. Morbi faucibus purus at lacus iaculis, a lobortis justo fermentum.</p>\r\n<p>In sed leo posuere tortor vulputate placerat. Suspendisse potenti. Morbi posuere dui ut augue lacinia, et imperdiet ex tempor. Sed ut turpis eleifend, semper ante mattis, lobortis elit. Aliquam fermentum tristique erat ac luctus. Phasellus suscipit sem quis libero dignissim, vitae sagittis nisl auctor. Quisque euismod mauris quis justo eleifend dignissim. Integer interdum nulla eget leo bibendum, eu laoreet nisi tempor. Ut a arcu vitae mi malesuada varius. Fusce faucibus magna ante, id scelerisque nulla scelerisque quis. Nullam commodo vitae orci eu tincidunt. Praesent rutrum consequat diam a sodales. Donec finibus finibus nisl id tristique. Curabitur aliquet semper fringilla.</p>\r\n<p>Nullam finibus et sem quis lobortis. Morbi dapibus bibendum sapien, sed auctor leo sagittis eget. Mauris leo odio, congue vel elementum nec, rhoncus et enim. Duis mi odio, vestibulum nec eros sed, consequat hendrerit ipsum. Suspendisse at sapien ante. Proin vitae aliquam tellus, vulputate scelerisque ex. Vivamus luctus elementum tempus. Sed sed ultrices augue.</p>', 2000, 750, 1, 23),
(24, 'Etiam efficitur justo', '2015-10-31', '2016-09-16', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mollis odio sit amet iaculis suscipit. Sed sit amet dui quis metus dictum ullamcorper. Etiam efficitur justo id leo ultricies, eu suscipit neque laoreet. Integer vitae imperdiet dui. Duis eget efficitur erat. Aenean libero velit, iaculis et lorem ut, aliquam maximus purus. Duis metus lacus, interdum at libero quis, bibendum facilisis leo. Duis in fermentum sem, eu suscipit mauris. Praesent facilisis nec nisl eget maximus. Etiam sem ipsum, consectetur ac risus et, viverra vestibulum elit. Cras molestie metus ut tellus dapibus, id facilisis elit bibendum. Nam tristique enim vel urna ullamcorper dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris congue sem erat, non semper lacus consectetur at. Aliquam blandit vel mi nec tempor.</p>\r\n<p>Mauris congue pharetra facilisis. Aenean commodo porttitor consectetur. Nulla feugiat, ipsum ornare lobortis pretium, eros nulla hendrerit ligula, vitae rutrum lorem felis ac erat. Proin magna tellus, consectetur sit amet iaculis sit amet, fringilla vitae justo. Donec non dui at magna lobortis faucibus eget vitae dolor. Vestibulum sit amet blandit augue. Curabitur sed molestie lacus, ac vehicula dolor. Nam dolor lorem, lacinia vel ex nec, semper blandit urna. Duis ut nulla egestas, egestas urna sit amet, sodales augue.</p>\r\n<p>Nullam felis quam, laoreet sit amet iaculis a, aliquam quis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris id metus faucibus, porttitor ligula vitae, ornare arcu. Quisque pulvinar ultrices mi, id rhoncus lectus bibendum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a massa arcu. In vel dapibus nisi. Donec blandit rhoncus felis, nec rutrum dui volutpat ut. Vestibulum vehicula luctus bibendum. In pharetra tellus ac ex fermentum aliquam. Maecenas molestie cursus orci vestibulum efficitur.</p>\r\n<p>Suspendisse eleifend eleifend mauris, at consequat erat gravida eu. Sed gravida lacus non mauris iaculis, a consequat sem congue. Cras sed tortor lectus. Fusce nec libero ultricies, luctus urna a, commodo purus. In hac habitasse platea dictumst. Cras pulvinar pharetra tempor. Integer ac tortor quis lacus rutrum molestie id vel est. Proin condimentum pulvinar lacus, eget tincidunt arcu semper vel. Aliquam erat volutpat. Donec id tortor consectetur, semper orci eu, rhoncus nibh. Cras dictum elementum tellus, non vehicula justo lacinia quis. Aliquam felis lorem, hendrerit non elementum vitae, mattis in tellus.</p>', 55555, 300, 2, 27),
(25, 'Titre du projet', '2015-11-02', '2016-11-16', '<p>In a augue non dui imperdiet egestas. Etiam congue nulla in purus accumsan imperdiet. Aenean accumsan, ligula dignissim ullamcorper elementum, lectus nisi mattis felis, nec fringilla ante dolor sollicitudin ipsum. Vivamus molestie est nisl, non eleifend urna porta at. Quisque convallis sapien ex, rhoncus luctus quam pulvinar sed. Duis rutrum sollicitudin mi. Cras euismod lectus a blandit viverra. Nullam in enim eget sem luctus laoreet in at odio. Quisque id venenatis urna, sed euismod ante.</p>\r\n<p>Quisque placerat interdum nulla, non lobortis mi vehicula at. Nunc auctor est malesuada tortor pellentesque, at luctus justo sodales. Proin orci lorem, tincidunt vitae malesuada eu, molestie vel massa. Sed vulputate semper consequat. Etiam venenatis, leo fermentum pulvinar dictum, lorem tellus malesuada tellus, sed maximus justo nisi quis sem. Fusce dignissim a nisl ut facilisis. Donec in ultricies arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nulla velit, aliquam vel dignissim vel, placerat a massa. Praesent consectetur viverra lacus non accumsan. Quisque eget turpis efficitur augue blandit imperdiet. Nulla elementum urna eget sem hendrerit auctor. Donec eget lacus velit. In dictum, lectus eu mollis gravida, ipsum leo viverra tortor, eget gravida dolor eros ac ligula.</p>\r\n<p>Suspendisse posuere volutpat lobortis. Nam ac turpis arcu. Ut facilisis diam in leo maximus, sed feugiat augue mattis. Aliquam luctus faucibus suscipit. Aenean ultricies nisi vitae odio sollicitudin elementum. Aliquam erat volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent in ex luctus, volutpat turpis nec, eleifend quam. Aenean elit nunc, hendrerit quis justo id, pulvinar suscipit lacus. Sed bibendum elementum urna, quis ullamcorper augue dignissim et. Pellentesque aliquet tellus id eros pretium, ac scelerisque massa posuere. Integer mattis erat vitae eros mattis, id consectetur ipsum gravida. Nullam enim lacus, porta in ligula ut, rutrum condimentum augue. Curabitur ac felis pulvinar, viverra est non, condimentum odio. Pellentesque vitae neque in risus auctor dapibus id a elit. Aenean ultricies sit amet sem eget ullamcorper.</p>\r\n<p>Vestibulum vel aliquet nulla, quis gravida risus. Fusce malesuada consectetur aliquet. Integer sollicitudin neque sit amet pretium ornare. Donec in ipsum sit amet mauris pulvinar aliquam sed fermentum nulla. Proin tristique mi quis odio placerat eleifend. Cras tristique purus quis rutrum commodo. Vivamus hendrerit lectus vitae ultricies dignissim. Nam semper in dui sit amet varius.</p>\r\n<p>Pellentesque vel sapien at nulla tempor egestas non quis dolor. In vestibulum hendrerit rutrum. Nunc quis blandit ante. Nulla ut tincidunt turpis. Nulla vulputate arcu nulla, in volutpat diam blandit at. Sed semper lorem massa, in lobortis sapien ornare eu. Nullam eu ante justo. Nullam tincidunt felis sed lobortis lobortis. Curabitur mollis neque ac leo posuere interdum. Vivamus scelerisque, magna ac dapibus euismod, sem dolor dictum massa, nec dictum nulla diam eget dolor. Donec placerat gravida viverra. Nam nec pharetra libero.</p>', 4, 0, 1, 28);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_naiss` date NOT NULL,
  `login` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pswd` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rules` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id_user`, `nom`, `prenom`, `mail`, `date_naiss`, `login`, `pswd`, `rules`) VALUES
(1, 'plop', 'plop', 'plop', '2015-06-15', 'plop', 'plop', 1),
(2, 'test', 'test', 'test', '2015-06-10', 'test', 'test', 1),
(3, 'Henriquel', 'Benjamin', 'bbb', '0000-00-00', 'BeniH', '$2y$12$m9WN.35shdXbLo5yIYHB/O37K.BDeWiI5jrAKH/OZdQTfHlUWdjOe', 0),
(4, 'yolo', 'yolo', 'yolo@yolo.yolo', '0000-00-00', 'yolo', '$2y$12$DJXU/O.PfjR/8pWn2S3l8Ohh8xRrJm/TyLOCTgXTodxcOpdLE92fq', 0),
(5, 'zerzerze', 'zertzert', 'azertrez@ijuyhtgrffghj.fr', '2222-02-02', 'xav', '$2y$12$VrcTRGVxv0avIF.6YLs3K.UswnyXT0s6EDp5sEQiy7rlDN6KomEy2', 0),
(11, 'Henriquel', 'Benjamin', 'Benjamin.Henriquel@gmail.com', '1994-10-19', 'BeniHen', '$2y$12$fcSTIien5U5JeKv/JqtTLuhULFt8O5or7N7g/M1Z/na5EgtDJUFXO', 0),
(13, 'Admin', 'Admin', 'Admin@Admin.Admin', '1111-11-11', 'Admin', '$2y$12$bRJvZrXR58OSFjaqLVR0ROXXIu0aRTMoCqncR8dUvcTvZ5s.cWvwC', 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `cadeau`
--
ALTER TABLE `cadeau`
  ADD CONSTRAINT `cadeau_ibfk_1` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id_projet`);

--
-- Contraintes pour la table `creation`
--
ALTER TABLE `creation`
  ADD CONSTRAINT `creation_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `creation_ibfk_2` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id_projet`);

--
-- Contraintes pour la table `financement`
--
ALTER TABLE `financement`
  ADD CONSTRAINT `financement_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `financement_ibfk_2` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id_projet`);

--
-- Contraintes pour la table `partenariat`
--
ALTER TABLE `partenariat`
  ADD CONSTRAINT `partenariat_ibfk_1` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id_projet`),
  ADD CONSTRAINT `partenariat_ibfk_2` FOREIGN KEY (`id_partenaire`) REFERENCES `partenaire` (`id_partenaire`);

--
-- Contraintes pour la table `projet`
--
ALTER TABLE `projet`
  ADD CONSTRAINT `projet_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id_categorie`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
