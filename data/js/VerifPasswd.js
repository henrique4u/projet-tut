/**
 * Created by BeniHen on 08/10/2015.
 */
function verifChangeMdp(){
    $funVerif = function() {
        if($("#newPasswd").val() != "")
            if ($("#verifNewPasswd").val() == $("#newPasswd").val()) {
                $("#divVerifPasswd").addClass("has-success").removeClass("has-warning");
                $("#changePswd").attr("disabled", false);
            } else {
                $("#divVerifPasswd").addClass("has-warning").removeClass("has-success");
                $("#changePswd").attr("disabled", true);
            }
    };

    $("#verifNewPasswd").keyup($funVerif);
    $("#newPasswd").keyup($funVerif);

};
