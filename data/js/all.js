/**
 * Created by BeniHen on 31/10/2015.
 */

function init() {
    $("#imgSearch").click(function () {
        if($("#searchBar input").val() ==""){
            $("#searchBar").animate({
                width:'toggle'
            },350);
            $("#divUsr").slideUp("fast");
        }else{
            $("#searchForm").submit();
        }
    });

    $("#imgUsr").click(function () {
        $("#divUsr").animate({
            height:'toggle'
        },350);
        if($("#searchBar").css("display") == "inline-table"){
            $("#searchBar").animate({
                width:'toggle'
            },250);
        }
    });


}

function delProj(id){
    if(confirm("Etes vous sur de vouloir surpimer ce projet")){
        window.location = "del/"+id;
    }
}