<?php
/**
* Created by PhpStorm.
 * User: BeniHenPCP
* Date: 22/06/2015
* Time: 09:12
*/

require 'vendor/autoload.php' ;
use \Slim\Slim as Slim;

\conf\DBConf::setConf();

session_start();


$app = new Slim();

/**
 * Partie qui affiche l'acceuil
 */
$app->get('/', function () {
    $controler = new \controler\Controler();
    $controler->accueil();
})->name('accueil');                     //accueil

/**
 * partie qui affiche un projet
 */

$app->get('/projet/:id', function ($id) {
    $controler = new \controler\Controler();
    $controler->afficheProjet($id);
})->name('projetId');

$app->get('/form/projet', function(){
    $controler = new \controler\Controler();
    $controler->formProjet();
})->name("formProjet");

$app->post('/form/projet', function(){
    $controler = new \controler\Controler();
    $controler->formProjetPart2();
})->name("formProjetPart2");

$app->get('/modif/projet/:id', function($id){
    $controler = new \controler\Controler();
    $controler->modifProjet($id);
})->name("changeProjet");

$app->post('/modif/projet/:id', function($id) {
    $controler = new \controler\AdminControler();
    $controler->modifProjet($id);
})->name("changeProjetAct");

$app->get('/listAll/:id', function($id){
    $controler = new \controler\Controler();
    $controler->allProjet($id);
})->name("allProjet");

$app->get('/listCateg/:id/:page', function($id, $page){
    $controler = new \controler\Controler();
    $controler->projectByCat($id, $page);
})->name("projectByCat");

$app->get('/form/compte', function () {
    $controler = new \vue\Form();
    $controler->addUserForm();
})->name('formCompte');

$app->post('/add/projet', function(){
    $controler = new \controler\AdminControler();
    $controler->addProjet();
})->name("addProjet");

$app->post('/mkCompte', function () {
    $controler = new \controler\AdminControler();
    $controler->creerCompte();
})->name('creerCompte');

$app->post('/connect', function () {
    $controler = new \controler\AdminControler();
    $controler->connect();
})->name('connection');

$app->post('/deco', function () {
    $controler = new \controler\AdminControler();
    $controler->disconnect();
})->name('deco');

$app->get('/financer', function () {
    $controler = new \controler\AdminControler();
    $controler->financer($_GET["idProj"], $_GET["somme"]);
})->name('financer');

$app->get('/compte', function () {
    $control = new \controler\Controler();
    $control->compte();
})->name('nomCompt');

$app->get('/changePasswd', function(){
    $render = new \vue\Form();
    $render->changeMdp();
})->name('changeMdp');

$app->post('/changedPasswd', function(){
    $act = new \controler\AdminControler();
    $act->changeMdp();
})->name("actionChangMdp");

$app->get('/changeMail', function(){
    $render = new \vue\Form();
    $render->changeMail();
})->name("changeMail");

$app->post('/changedMail',  function(){
    $control = new \controler\AdminControler();
    $control->changeMail();
})->name("actionChangeMail");

$app->get('/creerPartenaire', function(){
    $render = new \vue\Form();
    $render->creerPartenaire();
})->name("creationPartenaire");

$app->post('/add/partenaire', function(){
    $control = new \controler\AdminControler();
    $control->creerPartenaire();
})->name("actionCreationPartenaire");

$app->post('/add/reponce', function(){
    $control = new \controler\AdminControler();
    $control->repondre();
})->name("addReponce");

$app->post('/add/question', function(){
    $control = new \controler\AdminControler();
    $control->question();
})->name("addQuestion");

$app->get('/recherche', function(){
    $control = new \controler\Controler();
    $res = $control->search($_GET['sr']);
    $render = new \vue\render();
    $render->serch($res);
})->name("recherche");

$app->get('/association/:idProj/:idPart', function($idProj, $idPart){
    $controller = new \controler\AdminControler();
    $controller->associer($idProj,$idPart);
})->name("associer");

$app->get('/del/:id', function($id){
    $contriller = new \controler\AdminControler();
    $contriller->delProjet($id);
});

$app->run();